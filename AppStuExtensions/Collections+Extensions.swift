//
//  Collections.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation

//MARK: - Dictionaries
public func + <K, V> (left: [K : V], right: [K : V]) -> [K : V] {
    var newLeft = left
    for (k, v) in right {
        newLeft.updateValue(v, forKey: k)
    }
    return newLeft
}

public extension Dictionary where Value: Equatable {
    func allKeys(forValue val: Value) -> [Key] {
        return filter { $1 == val }.map { $0.0 }
    }
}

//MARK: - Array
public extension Array where Element: Equatable {
    mutating func appendIfUnique(element: Element) {
        if !contains(element) {
            append(element)
        }
    }
    
    mutating func remove(element: Element) {
        if let index = index(of: element) {
            remove(at: index)
        }
    }
}
