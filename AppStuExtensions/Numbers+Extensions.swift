//
//  Numbers.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation

//MARK: - Int
public extension Int {
    var isEven:Bool             {return (self % 2 == 0)}
    var isOdd:Bool              {return (self % 2 != 0)}
    var isPositive:Bool         {return (self >= 0)}
    var isNegative:Bool         {return (self < 0)}
    var toDouble:Double         {return Double(self)}
    var toFloat:Float           {return Float(self)}
    var toCGFloat:CGFloat       {return CGFloat(self)}
    var pixelsToPoints:CGFloat  {return CGFloat(self) / 2}
    
    var digits: Int {//this only works in bound of LONG_MAX 2147483647, the maximum value of int
        if self == 0 {
            return 1
        } else if(Int(fabs(Double(self))) <= LONG_MAX) {
            return Int(log10(fabs(Double(self)))) + 1
        } else {
            return -1; //out of bound
        }
    }
    
    func ordinalIndicatorString() -> String {
        switch self {
        case 1, 21, 31:
            return "\(self)st"
            
        case 2, 22:
            return "\(self)nd"
            
        case 3, 23:
            return "\(self)rd"
            
        default:
            return "\(self)th"
        }
    }
}

//MARK: - Double
public extension Double {
    func roundToDecimalDigits(decimals:Int) -> Double {
        let a : Double = self
        let format : NumberFormatter = NumberFormatter()
        format.numberStyle = NumberFormatter.Style.decimal
        format.roundingMode = NumberFormatter.RoundingMode.halfUp
        format.maximumFractionDigits = 2
        let string: NSString = format.string(from: NSNumber(value: a))! as NSString
        print(string.doubleValue)
        return string.doubleValue
    }
}

//MARK: - CGFloat
public extension CGFloat {
    func roundUp() -> CGFloat {
        return CGFloat(ceil(Double(self)))
    }
    
    func roundDown() -> CGFloat {
        return self.roundUp() - 1
    }
    
    static func onePixel() -> CGFloat {
        return 1.0 / UIScreen.main.scale
    }
}
