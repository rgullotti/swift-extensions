//
//  String.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation

public extension String {
    public var isNotEmpty: Bool {
        return !self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
    }
    
    public func contains(_ string: String, caseInsensitive: Bool = false) -> Bool {
        return caseInsensitive ? self.lowercased().contains(string.lowercased()) : self.contains(string)
    }
    
    public var length: Int {
        return self.characters.count
    }
    
    public var objcLength: Int {
        return self.utf16.count
    }
    
    public func makePluralIfNecessary() -> String {
        if characters.last != "s" {
            return self + "s"
        } else {
            return self
        }
    }
    
    public func makeSingularIfNecessary() -> String {
        if characters.last == "s" {
            return String(characters.dropLast())
        } else {
            return self
        }
    }
    
    //MARK: Linguistics
    
    /**
     Returns the langauge of a String
     
     NOTE: String has to be at least 4 characters, otherwise the method will return nil.
     
     - returns: String! Returns a string representing the langague of the string (e.g. en, fr, or und for undefined).
     */
    func detectLanguage() -> String! {
        if self.length > 4 {
            let tagger = {
                return NSLinguisticTagger(tagSchemes: [NSLinguisticTagSchemeLanguage], options: 0)
            }()
            tagger.string = self
            return tagger.tag(at: 0, scheme: NSLinguisticTagSchemeLanguage, tokenRange: nil, sentenceRange: nil)
        }
        return nil
    }
    
    /**
     Returns the script of a String
     
     - returns: String! returns a string representing the script of the String (e.g. Latn, Hans).
     */
    func detectScript() -> String! {
        let tagger = {
            return NSLinguisticTagger(tagSchemes: [NSLinguisticTagSchemeScript], options: 0)
        }()
        tagger.string = self
        return tagger.tag(at: 0, scheme: NSLinguisticTagSchemeScript, tokenRange: nil, sentenceRange: nil)
    }
    
    /**
     Check the text direction of a given String.
     
     NOTE: String has to be at least 4 characters, otherwise the method will return false.
     
     - returns: Bool The Bool will return true if the string was writting in a right to left langague (e.g. Arabic, Hebrew)
     
     */
    func isRightToLeft() -> Bool {
        let language = self.detectLanguage()
        return (language == "ar" || language == "he")
    }
    
    
    //MARK: Usablity & Social
    
    /**
     Check that a String is only made of white spaces, and new line characters.
     
     - returns: Bool
     */
    func isOnlyEmptySpacesAndNewLineCharacters() -> Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).length == 0
    }
    
    /**
     Checks if a string is an email address using NSDataDetector.
     
     - returns: Bool
     */
    var isEmail: Bool {
        let dataDetector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue),
        firstMatch = dataDetector.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.length))
        
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    /**
     Gets an array of URLs for all links found in a String
     
     - returns: [URL]
     */
    func getURLs() -> [URL] {
        let detector: NSDataDetector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let links = detector.matches(in: self, options: NSRegularExpression.MatchingOptions.withTransparentBounds, range: NSMakeRange(0, self.utf16.count))
        return links.filter { link in
            return link.url != nil
            }.map { link -> URL in
                return link.url!
        }
    }
    
    
    /**
     Gets an array of dates for all dates found in a String
     
     - returns: [Date]
     */
    func getDates() -> [Date] {
        let detector: NSDataDetector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.date.rawValue)
        let links = detector.matches(in: self, options: NSRegularExpression.MatchingOptions.withTransparentBounds, range: NSMakeRange(0, self.utf16.count))
        
        return links.filter { link in
            return link.date != nil
            }.map { link -> Date in
                return link.date!
        }
    }
    
    /**
     Gets an array of strings (hashtags #acme) for all links found in a String
     
     - returns: [String]
     */
    func getHashtags() -> [String] {
        let hashtagDetector = try! NSRegularExpression(pattern: "#(\\w+)", options: NSRegularExpression.Options.caseInsensitive),
        results = hashtagDetector.matches(in: self, options: NSRegularExpression.MatchingOptions.withoutAnchoringBounds, range: NSMakeRange(0, self.utf16.count))
        
        return results.map { textCheckingResult -> String in
            return self[textCheckingResult.rangeAt(0)]
        }
    }
    
    /**
     Gets an array of distinct strings (hashtags #acme) for all hashtags found in a String
     
     - returns: [String]
     */
    func getUniqueHashtags() -> [String] {
        return NSSet(array: self.getHashtags()).allObjects as! [String]
    }
    
    
    /**
     Gets an array of strings (mentions @apple) for all mentions found in a String
     
     - returns: [String]
     */
    func getMentions() -> [String] {
        let mentionDetector = try! NSRegularExpression(pattern: "@(\\w+)", options: NSRegularExpression.Options.caseInsensitive),
        results = mentionDetector.matches(in: self, options: NSRegularExpression.MatchingOptions.withoutAnchoringBounds, range: NSMakeRange(0, self.utf16.count))
        
        return results.map { textCheckingResult -> String in
            return self[textCheckingResult.rangeAt(0)]
        }
    }
    
    /**
     Check if a String contains a Date in it.
     
     - returns: Bool with true value if it does
     */
    func containsDate() -> Bool {
        return self.getDates().count > 0
    }
    
    /**
     - returns: Base64 encoded string
     */
    func encodeToBase64Encoding() -> String {
        let utf8str = self.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
        base64EncodedString = utf8str.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        return base64EncodedString
    }
    
    /**
     - returns: Decoded Base64 string
     */
    func decodeFromBase64Encoding() -> String {
        let base64data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0))!,
        decodedString = String(data: base64data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        return decodedString
    }
    
    /**
     Float value from a string
     */
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    // MARK: Subscript Methods
    
    subscript (i: Int) -> String {
        return String(Array(self.characters)[i])
    }
    
    subscript (r: Range<Int>) -> String {
        let start = self.index(startIndex, offsetBy: r.lowerBound),
        end = self.index(startIndex, offsetBy: r.upperBound)
        
        return substring(with: start ..< end)
    }
    
    subscript (range: NSRange) -> String {
        let end = range.location + range.length
        return self[range.location ..< end]
    }
    
    subscript (substring: String) -> Range<String.Index>? {
        return range(of: substring, options: String.CompareOptions.literal, range: startIndex ..< endIndex, locale: NSLocale.current)
    }
}
