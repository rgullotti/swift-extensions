//
//  Watch.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation
import WatchKit

public enum WatchResolution {
    case Watch38mm, Watch42mm, Unknown
}

public extension WKInterfaceDevice {
    class func currentResolution() -> WatchResolution {
        let watch38mmRect = CGRect(x: 0, y: 0, width: 136, height: 170)
        let watch42mmRect = CGRect(x: 0, y: 0, width: 156, height: 195)
        
        let currentBounds = WKInterfaceDevice.current().screenBounds
        
        switch currentBounds {
        case watch38mmRect:
            return .Watch38mm
        case watch42mmRect:
            return .Watch42mm
        default:
            return .Unknown
        }
    }
}
