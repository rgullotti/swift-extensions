//
//  CGRect.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation

public extension CGRect {
    func toCGImageRect() -> CGRect {
        let scale = UIScreen.main.scale
        return CGRect(x: self.origin.x * scale, y: self.origin.y * scale, width: self.size.width * scale, height: self.size.height * scale)
    }
}
