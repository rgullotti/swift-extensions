//
//  ViewControllers.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation
import UIKit
import CoreFoundation
import QuartzCore
private struct AssociatedKeys {
    static var currentKeyboardHeight = "currentKeyboardHeight"
}

//MARK: - View Controller
public extension UIViewController {
    func showAlert(title: String, message: String, actions: [UIAlertAction]?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let alertActions = actions {
            for action in alertActions {
                alert.addAction(action)
            }
        } else {
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                
            }))
        }
        present(alert, animated: true, completion: nil)
    }
    
    var currentKeyboardHeight: CGFloat! {
        get {
            if objc_getAssociatedObject(self, &AssociatedKeys.currentKeyboardHeight) == nil {
                objc_setAssociatedObject(self, &AssociatedKeys.currentKeyboardHeight, NSNumber(value: 0.0), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
            
            return CGFloat((objc_getAssociatedObject(self, &AssociatedKeys.currentKeyboardHeight) as! NSNumber).floatValue)
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.currentKeyboardHeight, NSNumber(value: Float(newValue)), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func adjustScrollViewForKeyboardDisplay(scrollView: UIScrollView) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil, using: { [weak self] (notification) in
            guard let info = notification.userInfo, let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size else { return }
            
            self?.currentKeyboardHeight = keyboardSize.height
            scrollView.contentInset.bottom += keyboardSize.height
            scrollView.scrollIndicatorInsets.bottom += keyboardSize.height
        })
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil, using: { [weak self] (notification) in
            if let keyboardHeight = self?.currentKeyboardHeight {
                scrollView.contentInset.bottom -= keyboardHeight
                scrollView.scrollIndicatorInsets.bottom -= keyboardHeight
            }
            self?.currentKeyboardHeight = 0.0
        })
    }
}

//MARK: - Collection View Controller
public extension UICollectionView {
    var collectionViewUsableWidth: CGFloat {
        get {
            if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
                let inset = flowLayout.sectionInset
                return frame.width - inset.right - inset.left
            } else {
                return frame.width
            }
        }
    }
    
    func register<T: UICollectionViewCell>(withoutNib: T.Type) where T: ReusableView {
        register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func register<T: UICollectionViewCell>(withNib: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func register<T: UICollectionViewCell>(classes: [T.Type]) where T: ReusableView, T: NibLoadableView {
        for className in classes {
            let bundle = Bundle(for: className.self)
            let nib = UINib(nibName: className.nibName, bundle: bundle)
            register(nib, forCellWithReuseIdentifier: className.defaultReuseIdentifier)
        }
    }
    
    func dequeue<T: UICollectionViewCell>(with className: T.Type, for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: className.defaultReuseIdentifier, for: indexPath as IndexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(className.defaultReuseIdentifier)")
        }
        return cell
    }

}

//MARK: - Table View Controller
public extension UITableView {
    //MARK: Cell Helpers
    func register<T: UITableViewCell>(withoutNib: T.Type) where T: ReusableView {
        register(T.self, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }

    func register<T: UITableViewCell>(withNib: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func register<T: UITableViewCell>(classes: [T.Type]) where T: ReusableView, T: NibLoadableView {
        for className in classes {
            let bundle = Bundle(for: className.self)
            let nib = UINib(nibName: className.nibName, bundle: bundle)
            register(nib, forCellReuseIdentifier: className.defaultReuseIdentifier)
        }
    }
    
    func dequeue<T: UITableViewCell>(with className: T.Type, for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableCell(withIdentifier: className.defaultReuseIdentifier, for: indexPath as IndexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(className.defaultReuseIdentifier)")
        }
        return cell
    }

    //MARK: Header/Footer Helpers
    func register<T: UITableViewHeaderFooterView>(headerFooter: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        self.register(nib, forHeaderFooterViewReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeue<T: UITableViewHeaderFooterView>(headerFooter named: T.Type) -> T where T: ReusableView {
        guard let headerFooter = dequeueReusableHeaderFooterView(withIdentifier: named.defaultReuseIdentifier) as? T else {
            fatalError("Could not dequeue header or footer with identifier: \(named.defaultReuseIdentifier)")
        }
        return headerFooter
        
    }
}

//MARK: - Tab Bar Controller
public extension UITabBarController {
    func removeItemTitles() {
        for item in tabBar.items ?? [] {
            item.title = ""
            item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        }
    }
}

//MARK: - Helpers for reusable cells

public protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

public extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

public protocol NibLoadableView: class {
    static var nibName: String { get }
}

public extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}

public extension UIViewController {
    static var classNameAsSwiftString: String {
        return String(describing: self)
    }
}

extension UICollectionViewCell: NibLoadableView, ReusableView {}

extension UITableViewCell: NibLoadableView, ReusableView {}

extension UITableViewHeaderFooterView: NibLoadableView, ReusableView {}

