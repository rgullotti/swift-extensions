//
//  Colors.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation

public extension UIColor {
    convenience init(hexString: String) {
        let rHex = hexString.substring(with: hexString.index(hexString.startIndex, offsetBy: 0)..<hexString.index(hexString.endIndex, offsetBy: -4))
        let gHex = hexString.substring(with: hexString.index(hexString.startIndex, offsetBy: 2)..<hexString.index(hexString.endIndex, offsetBy: -2))
        let bHex = hexString.substring(with: hexString.index(hexString.startIndex, offsetBy: 4)..<hexString.index(hexString.endIndex, offsetBy: 0))
        self.init(red: UInt(rHex, radix: 16)!, green: UInt(gHex, radix: 16)!, blue: UInt(bHex, radix: 16)!)
    }
    
    convenience init(red: UInt, green: UInt, blue: UInt) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}

public func getColorThatShowsOnTopOf(color: UIColor) -> UIColor {
    var returnColor = UIColor.black
    
    let componentColors = color.cgColor.components
    
    let redColor = Double((componentColors?[0])!) * 299
    let greenColor = Double((componentColors?[1])!) * 587
    let blueColor = Double((componentColors?[2])!) * 114
    let colorBrightness = (redColor + greenColor + blueColor) / 1000
    if (colorBrightness < 0.5) {
        // Color is dark
        returnColor = UIColor.white
    } else {
        // Color is light
        returnColor = UIColor.black
    }
    
    return returnColor
}
