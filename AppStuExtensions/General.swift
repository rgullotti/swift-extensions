//
//  General.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation

public func delay(delay:Double, completion:@escaping ()->()) {
    let deadlineTime = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
        completion()
    }
}



