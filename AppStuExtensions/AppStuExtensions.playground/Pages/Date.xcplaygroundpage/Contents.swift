//: Playground - noun: a place where people can play

import UIKit
import AppStuExtensions

let today = Date()
let tomorrow = today.adjusted(by: 1, component: .day)
let yesterday = today.adjusted(by: -1, component: .day)


today.component(.hour, between: tomorrow)

