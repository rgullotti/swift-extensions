//
//  Date.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

public enum DateFormat {
    case ISO8601, DotNet, RSS, AltRSS
    case Custom(String)
}

public extension Date {
    
    //MARK: - Intervals In Seconds
    private static func minuteInSeconds() -> Double { return 60 }
    private static func hourInSeconds() -> Double { return 3600 }
    private static func dayInSeconds() -> Double { return 86400 }
    private static func weekInSeconds() -> Double { return 604800 }
    private static func yearInSeconds() -> Double { return 31556926 }
    
    //MARK: - Components
    private static func componentFlags() -> Set<Calendar.Component> {
        return [.year, .month, .day, .weekOfMonth, .hour, .minute, .second, .weekday, .weekdayOrdinal, .weekOfYear]
    }
    
    private static func basicComponentFlags() -> Set<Calendar.Component> {
        return [.year, .month, .day]
    }
    
    static func components(fromDate: Date) -> DateComponents {
        return Calendar.current.dateComponents(Date.componentFlags(), from: fromDate)
    }
    
    func components() -> DateComponents  {
        return Date.components(fromDate: self)
    }
    
    //MARK: - Decomposing Dates
    public var second: Int { return self.components().second! }
    public var minute: Int { return self.components().minute! }
    public var hour: Int { return self.components().hour! }
    public var day: Int { return self.components().day! }
    public var week: Int { return self.components().weekOfYear! }
    public var weekOfMonth: Int { return self.components().weekOfMonth! }
    public var weekOfYear: Int { return self.components().weekOfYear! }
    public var month: Int { return self.components().month! }
    public var year: Int { return self.components().year! }
    public var weekday: Int { return self.components().weekday! }
    public var nthWeekday: Int { return self.components().weekdayOrdinal! } //// e.g. 2nd Tuesday of the month is 2
    public var nearestHour: Int {
        if  self.minute < 30 {
            return self.hour
        } else {
            return self.hour + 1
        }
    }
    public var monthDays: Int {
        let range = Calendar.current.range(of: .day, in: .month, for: self)!
        return range.upperBound - range.lowerBound
    }
    public var firstDayOfWeek: Int {
        let distanceToStartOfWeek = Date.dayInSeconds() * Double(self.components().weekday! - 1)
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate - distanceToStartOfWeek
        return Date(timeIntervalSinceReferenceDate: interval).day
    }
    public var lastDayOfWeek: Int {
        let distanceToStartOfWeek = Date.dayInSeconds() * Double(self.components().weekday! - 1)
        let distanceToEndOfWeek = Date.dayInSeconds() * Double(7)
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate - distanceToStartOfWeek + distanceToEndOfWeek
        return Date(timeIntervalSinceReferenceDate: interval).day
    }
    public var isWeekday: Bool {
        return !self.isWeekendDay
    }
    public var isWeekendDay: Bool {
        return Calendar.current.isDateInWeekend(self)
    }

    //MARK: - Amount between dates
    public func component(_ component: Calendar.Component, between date: Date, signed: Bool = true) -> Int {
        let returnValue = Calendar.current.dateComponents([component], from: self, to: date).value(for: component)!
        return signed ? returnValue : abs(returnValue)
    }

    //MARK: - Comparing Dates
    public func isSame(_ component: Calendar.Component, as date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: component)
    }
    
    public func isToday() -> Bool {
        return self.isSame(.day, as: Date())
    }
    
    public func isTomorrow() -> Bool {
        return self.isSame(.day, as: Date().adjusted(by: 1, component: .day))
    }
    
    public func isYesterday() -> Bool {
        return self.isSame(.day, as: Date().adjusted(by: -1, component: .day))
    }
    
    public func isThisWeek() -> Bool {
        return self.isSame(.weekOfYear, as: Date())
    }
    
    public func isNextWeek() -> Bool {
        return self.isSame(.weekOfYear, as: Date())
    }
    
    public func isLastWeek() -> Bool {
        return self.isSame(.weekOfYear, as: Date())
    }
    
    public func isThisMonth() -> Bool {
        return self.isSame(.month, as: Date())
    }
    
    public func isThisYear() -> Bool {
        return self.isSame(.year, as: Date())
    }
    
    public func isNextYear() -> Bool {
        let comp1 = Date.components(fromDate: self)
        let comp2 = Date.components(fromDate: Date())
        return (comp1.year! == comp2.year! + 1)
    }
    
    public func isLastYear() -> Bool {
        let comp1 = Date.components(fromDate: self)
        let comp2 = Date.components(fromDate: Date())
        return (comp1.year! == comp2.year! - 1)
    }
    
    public func isBetween(startDate date: Date, endDate: Date) -> Bool {
        return self >= date && self <= endDate
    }
    
    //MARK: - Adjusting Dates
    public func adjusted(by value: Int, component: Calendar.Component) -> Date {
        return Calendar.current.date(byAdding: component, value: value, to: self)!
    }
    
    public mutating func adjust(by value: Int, component: Calendar.Component) {
        self = Calendar.current.date(byAdding: component, value: value, to: self)!
    }
    
    public func dateAtStartOfDay() -> Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    public func dateAtStartOfWeek() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .weekOfMonth, .weekday]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.weekday = 1 // Sunday
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    public func dateAtStartOfMonth() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }

    public func dateAtStartOfNextMonth() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 1
        components.month = components.month! + 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    public func dateAtStartOfYear() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 1
        components.month = 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    public func dateAtEndOfDay() -> Date {
        return self.dateAtStartOfDay().adjusted(by: 1, component: .day).adjusted(by: -1, component: .second)
    }
    
    public func dateAtEndOfWeek() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .weekOfMonth, .weekday]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.weekday = 7 // Saturday
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Calendar.current.date(from: components)!
    }
    
    public func dateAtEndOfMonth() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 0
        components.month = components.month! + 1
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Calendar.current.date(from: components)!
    }
    
    public func dateAtEndOfYear() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 31
        components.month = 12
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Calendar.current.date(from: components)!
    }
    
    public func dateBySettingWeekDayThisWeek(weekDay: Int) -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .weekday, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        let currentWeekday = self.weekday
        components.day = components.day! - (currentWeekday - weekDay)
        components.weekday = weekDay
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    public func dateBySettingDay(day: Int) -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = day
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    public func dateBySettingMonth(month: Int) -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.month = month
        components.day = 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    public func dateBySettingTime(hour: Int, minute: Int) -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.hour = hour
        components.minute = minute
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    public func isLastDayOfMonth() -> Bool {
        let calendar = NSCalendar.current
        let dayRange = calendar.range(of: .day, in: .month, for: self)
        let dayCount = dayRange!.upperBound - dayRange!.lowerBound
        var components = calendar.dateComponents([.year, .month, .day], from: self)
        
        components.day = dayCount
        
        let currentday = self.day
        return components.day == currentday
    }
    
    //MARK: - To String
    
    public func toString() -> String {
        return self.toString(dateStyle: .short, timeStyle: .short, doesRelativeDateFormatting: false)
    }
    
    public func toString(format: DateFormat) -> String {
        var dateFormat: String
        switch format {
        case .DotNet:
            let offset = NSTimeZone.default.secondsFromGMT() / 3600
            let nowMillis = 1000 * self.timeIntervalSince1970
            return  "/Date(\(nowMillis)\(offset))/"
        case .ISO8601:
            dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        case .RSS:
            dateFormat = "EEE, d MMM yyyy HH:mm:ss ZZZ"
        case .AltRSS:
            dateFormat = "d MMM yyyy HH:mm:ss ZZZ"
        case .Custom(let string):
            dateFormat = string
        }
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        return formatter.string(from: self)
    }
    
    public func toString(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style, doesRelativeDateFormatting: Bool = false) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = dateStyle
        formatter.timeStyle = timeStyle
        formatter.doesRelativeDateFormatting = doesRelativeDateFormatting
        return formatter.string(from: self)
    }
    
    public func toExpirationStringInFuture(futureDate: Date) -> String {
        let compareComponents = Calendar.current.dateComponents(Date.basicComponentFlags(), from: self, to: futureDate)
        //TODO:        let compareComponents = Calendar.current.dateComponents(components: Date.basicComponentFlags(), fromDate: self, toDate: futureDate, options: NSCalendar.Options.WrapComponents)
        
        var stringToReturn = ""
		
        if compareComponents.year! > 0 {
            stringToReturn += "\(compareComponents.year!) "
            if compareComponents.year == 1 {
                stringToReturn += "Year"
            } else {
                stringToReturn += "Years"
            }
            // if expiration date is in years then we probably don't need to show anymore precision than that
            return stringToReturn
        }
        
        if compareComponents.month! > 0 {
            stringToReturn += "\(compareComponents.month!) "
            if compareComponents.month == 1 {
                stringToReturn += "Month"
            } else {
                stringToReturn += "Months"
            }
            // if expiration date is in months then we probably don't need to show anymore precision than that
            return stringToReturn
        }
        
        if compareComponents.day! > 0 {
            stringToReturn += "\(compareComponents.day!) "
            if compareComponents.day == 1 {
                stringToReturn += "Day"
            } else {
                stringToReturn += "Days"
            }
            return stringToReturn
        }
        
        if compareComponents.day == 0 {
            // expires today
            return "Today"
        }
        
        // now check for minus expiration dates
        
        if compareComponents.year! < 0 {
            stringToReturn += "\(compareComponents.year! * -1) "
            if compareComponents.year == -1 {
                stringToReturn += "Year ago"
            } else {
                stringToReturn += "Years ago"
            }
            // if expiration date is in years then we probably don't need to show anymore precision than that
            return stringToReturn
        }
        
        if compareComponents.month! < 0 {
            stringToReturn += "\(compareComponents.month! * -1) "
            if compareComponents.month == -1 {
                stringToReturn += "Month ago"
            } else {
                stringToReturn += "Months ago"
            }
            // if expiration date is in months then we probably don't need to show anymore precision than that
            return stringToReturn
        }
        
        if compareComponents.day! < 0 {
            stringToReturn += "\(compareComponents.day! * -1) "
            if compareComponents.day == -1 {
                stringToReturn += "Day ago"
            } else {
                stringToReturn += "Days ago"
            }
            return stringToReturn
        }
        
        return stringToReturn
    }
    
    public func relativeTimeToString() -> String {
        let time = self.timeIntervalSince1970
        let now = Date().timeIntervalSince1970
        
        let seconds = now - time
        let minutes = round(seconds/60)
        let hours = round(minutes/60)
        let days = round(hours/24)
        
        if seconds < 10 {
            return NSLocalizedString("just now", comment: "relative time")
        } else if seconds < 60 {
            return NSLocalizedString("\(seconds) seconds ago", comment: "relative time")
        }
        
        if minutes < 60 {
            if minutes == 1 {
                return NSLocalizedString("1 minute ago", comment: "relative time")
            } else {
                return NSLocalizedString("\(minutes) minutes ago", comment: "relative time")
            }
        }
        
        if hours < 24 {
            if hours == 1 {
                return NSLocalizedString("1 hour ago", comment: "relative time")
            } else {
                return NSLocalizedString("\(hours) hours ago", comment: "relative time")
            }
        }
        
        if days < 7 {
            if days == 1 {
                return NSLocalizedString("1 day ago", comment: "relative time")
            } else {
                return NSLocalizedString("\(days) days ago", comment: "relative time")
            }
        }
        
        return self.toString()
    }
    
    public func dayWithOrdinalIndicatorString() -> String {
        let day = self.day
        switch day {
        case 1, 21, 31:
            return "\(day)st"
            
        case 2, 22:
            return "\(day)nd"
            
        case 3, 23:
            return "\(day)rd"
            
        default:
            return "\(day)th"
        }
    }
    
    
    public func weekdayToString() -> String {
        let formatter = DateFormatter()
        return formatter.weekdaySymbols[self.weekday-1]
    }
    
    public func shortWeekdayToString() -> String {
        let formatter = DateFormatter()
        return formatter.shortWeekdaySymbols[self.weekday-1]
    }
    
    public func veryShortWeekdayToString() -> String {
        let formatter = DateFormatter()
        return formatter.veryShortWeekdaySymbols[self.weekday-1]
    }
    
    public func monthToString() -> String {
        let formatter = DateFormatter()
        return formatter.monthSymbols[self.month-1]
    }
    
    public func shortMonthToString() -> String {
        let formatter = DateFormatter()
        return formatter.shortMonthSymbols[self.month-1]
    }
    
    public func veryShortMonthToString() -> String {
        let formatter = DateFormatter()
        return formatter.veryShortMonthSymbols[self.month-1]
    }
    
    //MARK: - Date From String
    public init(fromString string: String, format:DateFormat) {
        if string.isEmpty {
            self.init()
            return
        }
        
        let string = string
        
        switch format {
            
        case .DotNet:
            
            // Expects "/Date(1268123281843)/"
            let startIndex = string.range(of: "(")!.upperBound
            let endIndex = string.range(of: ")")!.lowerBound
            let range = startIndex ..< endIndex
            let milliseconds = (string.substring(with: range) as NSString).longLongValue
            let interval = TimeInterval(milliseconds / 1000)
            self.init(timeIntervalSince1970: interval)
            
        case .ISO8601:
            
            var s = string
            if string.hasSuffix(" 00:00") {
                s = s.substring(to: s.index(s.endIndex, offsetBy: -6)) + "GMT"
            } else if string.hasSuffix("Z") {
                s = s.substring(to: s.index(s.endIndex, offsetBy: -1)) + "GMT"
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
            if let date = formatter.date(from: string) {
                self.init()
                self = Date(timeInterval: 0, since: date)
                //                self(timeInterval:0, since:date)
            } else {
                self.init()
            }
            
        case .RSS:
            
            var s  = string
            if string.hasSuffix("Z") {
                //                s = s.substringToIndex(s.length-1) + "GMT"
                s = s.substring(to: s.index(s.endIndex, offsetBy: -1)) + "GMT"
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "EEE, d MMM yyyy HH:mm:ss ZZZ"
            if let date = formatter.date(from: string) {
                self.init()
                self = Date(timeInterval: 0, since: date)
                //                self(timeInterval:0, since:date)
            } else {
                self.init()
            }
            
        case .AltRSS:
            
            var s  = string
            if string.hasSuffix("Z") {
                //                s = s.substringToIndex(s.length-1) + "GMT"
                s = s.substring(to: s.index(s.endIndex, offsetBy: -1)) + "GMT"
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "d MMM yyyy HH:mm:ss ZZZ"
            if let date = formatter.date(from: string) {
                self.init()
                self = Date(timeInterval: 0, since: date)
                //                self(timeInterval:0, since:date)
            } else {
                self.init()
            }
            
        case .Custom(let dateFormat):
            
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat
            if let date = formatter.date(from: string) {
                self.init()
                self = Date(timeInterval: 0, since: date)
                //                self(timeInterval:0, since:date)
            } else {
                self.init()
            }
        }
    }

}

