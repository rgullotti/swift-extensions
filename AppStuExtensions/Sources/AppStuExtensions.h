//
//  AppStuExtensions.h
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

#include <TargetConditionals.h>

#if TARGET_OS_IPHONE || TARGET_OS_TV || TARGET_IPHONE_SIMULATOR
#import <UIKit/UIKit.h>
#else
#import <Cocoa/Cocoa.h>
#endif

//! Project version number for AppStuExtensions.
FOUNDATION_EXPORT double AppStuExtensionVersionNumber;

//! Project version string for AppStuExtensions.
FOUNDATION_EXPORT const unsigned char AppStuExtensionVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AppStuExtensions/PublicHeader.h>


