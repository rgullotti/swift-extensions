//
//  Views.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 9/14/16.
//
//

import Foundation

//MARK: - Image View
public extension UIImageView {
    func roundImage() {
        //height and width should be the same
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.width / 2;
    }
}

//MARK: - Image
public extension UIImage {
    func croppedImage(bound : CGRect) -> UIImage {
        let scaledBounds = CGRect(x: bound.origin.x * self.scale, y: bound.origin.y * self.scale, width: bound.size.width * self.scale, height: bound.size.height * self.scale)
        let imageRef = self.cgImage!.cropping(to: scaledBounds)
        let croppedImage : UIImage = UIImage(cgImage: imageRef!, scale: self.scale, orientation: UIImageOrientation.up)
        return croppedImage;
    }
}


//MARK: - Stack View
public extension UIStackView {
    func addSpacingView(spacing: CGFloat) {
        let spacingView = UIView()
        if axis == .vertical {
            spacingView.anchor().height(constant: spacing).activate()
        } else {
            spacingView.anchor().width(constant: spacing).activate()
        }
        self.addArrangedSubview(spacingView)
    }
    
}

//MARK: - View
open class UIViewWithXib: UIView {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    open func setupView() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing:type(of: self)), bundle: bundle)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else { return }
        view.frame = bounds
        addSubview(view)
    }
}

public extension UIView {
    //MARK: Convenience Initializers
    convenience init(with children: [UIView]) {
        self.init()
        children.forEach { (child) in
            addSubview(child)
        }
    }
    
    //MARK: Separators
    public func addHorizontalSeparatorLine(to toSide: NSLayoutYAxisAnchor, leftMargin left: CGFloat = 0.0, rightMargin right: CGFloat = 0.0, color: UIColor = UIColor.lightGray, thickness: CGFloat = CGFloat.onePixel()) -> UIView {
        return addSeparatorLine(to: toSide, leftMargin: left, rightMargin: right, color: color, thickness: thickness)
    }
    
    public func addVerticalSeparatorLine(to toSide: NSLayoutXAxisAnchor, topMargin left: CGFloat = 0.0, bottomMargin right: CGFloat = 0.0, color: UIColor = UIColor.lightGray, thickness: CGFloat = CGFloat.onePixel()) -> UIView {
        return addSeparatorLine(to: toSide, leftMargin: left, rightMargin: right, color: color, thickness: thickness)
    }
    
    private func addSeparatorLine(to toSide: AnyObject, leftMargin left: CGFloat = 0.0, rightMargin right: CGFloat = 0.0, color: UIColor = UIColor.lightGray, thickness: CGFloat = CGFloat.onePixel()) -> UIView {
        let separatorLine = UIView()
        separatorLine.backgroundColor = color
        separatorLine.alpha = 1.0
        addSubview(separatorLine)
        guard let toSide = toSide as? NSLayoutAnchor<AnyObject> else { return UIView() }
        if toSide == topAnchor {
            separatorLine.anchor().leftToSuperview(constant: left).rightToSuperview(constant: right).topToSuperview().height(constant: thickness).activate()
            
        } else if toSide == bottomAnchor {
            separatorLine.anchor().leftToSuperview(constant: left).rightToSuperview(constant: right).bottomToSuperview().height(constant: thickness).activate()
            
        } else if toSide == leftAnchor {
            separatorLine.anchor().topToSuperview(constant: left).bottomToSuperview(constant: right).leftToSuperview().width(constant: thickness).activate()
            
        } else if toSide == rightAnchor {
            separatorLine.anchor().topToSuperview(constant: left).bottomToSuperview(constant: right).rightToSuperview().width(constant: thickness).activate()
            
        }
        
        return separatorLine
    }
}

//MARK: - Layout Extensions
public extension UIView {
    public func anchor() -> Anchor { return Anchor(view: self) }
}

public struct Anchor {
    public let view: UIView
    public let top: NSLayoutConstraint?
    public let left: NSLayoutConstraint?
    public let bottom: NSLayoutConstraint?
    public let right: NSLayoutConstraint?
    public let height: NSLayoutConstraint?
    public let width: NSLayoutConstraint?
    public let centerX: NSLayoutConstraint?
    public let centerY: NSLayoutConstraint?
    
    internal init(view: UIView) {
        self.view = view
        top = nil
        left = nil
        bottom = nil
        right = nil
        height = nil
        width = nil
        centerX = nil
        centerY = nil
    }
    
    private init(view: UIView,
                 top: NSLayoutConstraint?,
                 left: NSLayoutConstraint?,
                 bottom: NSLayoutConstraint?,
                 right: NSLayoutConstraint?,
                 height: NSLayoutConstraint?,
                 width: NSLayoutConstraint?,
                 centerX: NSLayoutConstraint?,
                 centerY: NSLayoutConstraint?) {
        self.view = view
        self.top = top
        self.left = left
        self.bottom = bottom
        self.right = right
        self.height = height
        self.width = width
        self.centerX = centerX
        self.centerY = centerY
    }
    
    private func update(edge: NSLayoutAttribute, constraint: NSLayoutConstraint?) -> Anchor {
        var top = self.top
        var left = self.left
        var bottom = self.bottom
        var right = self.right
        var height = self.height
        var width = self.width
        var centerX = self.centerX
        var centerY = self.centerY
        
        switch edge {
        case .top: top = constraint
        case .left: left = constraint
        case .bottom: bottom = constraint
        case .right: right = constraint
        case .height: height = constraint
        case .width: width = constraint
        case .centerX: centerX = constraint
        case .centerY: centerY = constraint
        default: return self
        }
        
        return Anchor(
            view: self.view,
            top: top,
            left: left,
            bottom: bottom,
            right: right,
            height: height,
            width: width,
            centerX: centerX,
            centerY: centerY)
    }
    
    private func insert(anchor: Anchor) -> Anchor {
        return Anchor(
            view: self.view,
            top: anchor.top ?? top,
            left: anchor.left ?? left,
            bottom: anchor.bottom ?? bottom,
            right: anchor.right ?? right,
            height: anchor.height ?? height,
            width: anchor.width ?? width,
            centerX: anchor.centerX ?? centerX,
            centerY: anchor.centerY ?? centerY)
    }
    
    //MARK: Hugging
    public func hugContentWidth() -> Anchor {
        view.setContentHuggingPriority(UILayoutPriorityRequired, for: .horizontal)
        return self
    }
    
    public func hugContentHeight() -> Anchor {
        view.setContentHuggingPriority(UILayoutPriorityRequired, for: .vertical)
        return self
    }
    
    // MARK: Anchor to superview edges
    public func topToSuperview(constant c: CGFloat = 0) -> Anchor {
        guard let superview = view.superview else {
            return self
        }
        
        return top(to: superview.topAnchor, constant: c)
    }
    
    public func leftToSuperview(constant c: CGFloat = 0) -> Anchor {
        guard let superview = view.superview else {
            return self
        }
        return left(to: superview.leftAnchor, constant: c)
    }
    
    public func bottomToSuperview(constant c: CGFloat = 0) -> Anchor {
        guard let superview = view.superview else {
            return self
        }
        return bottom(to: superview.bottomAnchor, constant: c)
    }
    
    public func rightToSuperview(constant c: CGFloat = 0) -> Anchor {
        guard let superview = view.superview else {
            return self
        }
        return right(to: superview.rightAnchor, constant: c)
    }
    
    public func edgesToSuperview(omitEdge e: NSLayoutAttribute = .notAnAttribute, insets: UIEdgeInsets = UIEdgeInsets.zero) -> Anchor {
        let superviewAnchors = topToSuperview(constant: insets.top)
            .leftToSuperview(constant: insets.left)
            .bottomToSuperview(constant: insets.bottom)
            .rightToSuperview(constant: insets.right)
            .update(edge: e, constraint: nil)
        return self.insert(anchor: superviewAnchors)
    }
    
    // MARK: Anchor to superview axises
    public func centerXToSuperview() -> Anchor {
        guard let superview = view.superview else {
            return self
        }
        return centerX(to: superview.centerXAnchor)
    }
    
    public func centerYToSuperview() -> Anchor {
        guard let superview = view.superview else {
            return self
        }
        return centerY(to: superview.centerYAnchor)
    }
    
    public func centerToSuperview() -> Anchor {
        guard let superview = view.superview else {
            return self
        }
        return centerX(to: superview.centerXAnchor)
            .centerY(to: superview.centerYAnchor)
    }
    
    // MARK: Anchor to
    public func top(to anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .top, constraint: view.topAnchor.constraint(equalTo: anchor, constant: c))
    }
    
    public func left(to anchor: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .left, constraint: view.leftAnchor.constraint(equalTo: anchor, constant: c))
    }
    
    public func bottom(to anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .bottom, constraint: view.bottomAnchor.constraint(equalTo: anchor, constant: c))
    }
    
    public func right(to anchor: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .right, constraint: view.rightAnchor.constraint(equalTo: anchor, constant: c))
    }
    
    // MARK: Anchor greaterOrEqual
    public func top(greaterOrEqual anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .top, constraint: view.topAnchor.constraint(greaterThanOrEqualTo: anchor, constant: c))
    }
    
    public func left(greaterOrEqual anchor: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .left, constraint: view.leftAnchor.constraint(greaterThanOrEqualTo: anchor, constant: c))
    }
    
    public func bottom(greaterOrEqual anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .bottom, constraint: view.bottomAnchor.constraint(greaterThanOrEqualTo: anchor, constant: c))
    }
    
    public func right(greaterOrEqual anchor: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .right, constraint: view.rightAnchor.constraint(greaterThanOrEqualTo: anchor, constant: c))
    }
    
    // MARK: Anchor lessOrEqual
    public func top(lesserOrEqual anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .top, constraint: view.topAnchor.constraint(lessThanOrEqualTo: anchor, constant: c))
    }
    
    public func left(lesserOrEqual anchor: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .left, constraint: view.leftAnchor.constraint(lessThanOrEqualTo: anchor, constant: c))
    }
    
    public func bottom(lesserOrEqual anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .bottom, constraint: view.bottomAnchor.constraint(lessThanOrEqualTo: anchor, constant: c))
    }
    
    public func right(lesserOrEqual anchor: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .right, constraint: view.rightAnchor.constraint(lessThanOrEqualTo: anchor, constant: c))
    }
    
    // MARK: Dimension anchors
    public func height(constant c: CGFloat) -> Anchor {
        return update(edge: .height, constraint: view.heightAnchor.constraint(equalToConstant: c))
    }
    
    public func height(to dimension: NSLayoutDimension, multiplier m: CGFloat = 1) -> Anchor {
        return update(edge: .height, constraint: view.heightAnchor.constraint(equalTo: dimension, multiplier: m))
    }
    
    public func width(constant c: CGFloat) -> Anchor {
        return update(edge: .width, constraint: view.widthAnchor.constraint(equalToConstant: c))
    }
    
    public func width(to dimension: NSLayoutDimension, multiplier m: CGFloat = 1) -> Anchor {
        return update(edge: .width, constraint: view.widthAnchor.constraint(equalTo: dimension, multiplier: m))
    }
    
    // MARK: Axis anchors
    public func centerX(to axis: NSLayoutXAxisAnchor, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .centerX, constraint: view.centerXAnchor.constraint(equalTo: axis, constant: c))
    }
    
    public func centerY(to axis: NSLayoutYAxisAnchor, constant c: CGFloat = 0) -> Anchor {
        return update(edge: .centerY, constraint: view.centerYAnchor.constraint(equalTo: axis, constant: c))
    }
    
    // MARK: Activation
    @discardableResult public func activate() -> Anchor {
        view.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [top, left, bottom, right, height, width, centerX, centerY].flatMap({ $0 })
        NSLayoutConstraint.activate(constraints)
        return self
    }
}

//MARK: Helpers

public extension UIView{
    
    class func loadFromNibNamed(nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(nibName: nibNamed, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    // MARK: Variables
    var width:      CGFloat { return self.frame.size.width }
    var height:     CGFloat { return self.frame.size.height }
    var size:       CGSize  { return self.frame.size}
    
    var origin:     CGPoint { return self.frame.origin }
    var centerX:    CGFloat { return self.center.x }
    var centerY:    CGFloat { return self.center.y }
    
    var left:       CGFloat { return self.frame.origin.x }
    var right:      CGFloat { return self.frame.origin.x + self.frame.size.width }
    var top:        CGFloat { return self.frame.origin.y }
    var bottom:     CGFloat { return self.frame.origin.y + self.frame.size.height }
    
    // MARK: Functions
    // MARK: Positioning and Size
    
    func setWidth(width:CGFloat) {
        self.frame.size.width = width
    }
    
    func setHeight(height:CGFloat) {
        self.frame.size.height = height
    }
    
    func setSize(size:CGSize) {
        self.frame.size = size
    }
    
    func setOrigin(point:CGPoint) {
        self.frame.origin = point
    }
    
    //only change the origin x
    func setX(x:CGFloat) {
        self.frame.origin = CGPoint(x: x, y: self.frame.origin.y)
    }
    
    //only change the origin y
    func setY(y:CGFloat) {
        self.frame.origin = CGPoint(x: self.frame.origin.x, y: y)
    }
    
    //only change the center x
    func setCenterX(x:CGFloat) {
        self.center = CGPoint(x: x, y: self.center.y)
    }
    
    //only change the center y
    func setCenterY(y:CGFloat) {
        self.center = CGPoint(x: self.center.x, y: y)
    }
    
    func roundCorner(radius:CGFloat) {
        self.layer.cornerRadius = radius
    }
    
    func setTop(top:CGFloat) {
        self.frame.origin.y = top
    }
    
    func setLeft(left:CGFloat) {
        self.frame.origin.x = left
    }
    
    func setRight(right:CGFloat) {
        self.frame.origin.x = right - self.frame.size.width
    }
    
    func setBottom(bottom:CGFloat) {
        self.frame.origin.y = bottom - self.frame.size.height
    }
    
    func centerInSuperview() {
        if superview == nil {
            return
        }
        center = CGPoint(x: superview!.centerX, y: superview!.centerY)
    }
    
    func centerHorizontally() {
        if superview == nil {
            return
        }
        center = CGPoint(x: superview!.centerX, y: origin.y)
    }
    
    func centerVertically() {
        if superview == nil {
            return
        }
        center = CGPoint(x: origin.x, y: superview!.centerY)
    }
    
    //MARK: Calculation Functions
    func percentageOfPan(offset: CGFloat) -> CGFloat {
        var percentage = offset / self.width
        
        if percentage < -1.0 {
            percentage = -1.0
        } else if percentage > 1.0 {
            percentage = 1.0
        }
        
        if percentage < 0 {
            percentage = percentage * -1
        }
        
        return percentage
    }
    
    // MARK: Border
    
    func borderColor() -> UIColor { return UIColor(cgColor: layer.borderColor!) }
    func borderColor(borderColor: UIColor) { layer.borderColor = borderColor.cgColor }
    
    func borderWidth() -> CGFloat { return layer.borderWidth }
    func borderWidth(borderWidth: CGFloat) { layer.borderWidth = borderWidth }
    
    func borderWithDashPattern(lineDashPattern: [Int]?, borderWidth: CGFloat, borderColor: UIColor, cornerRadius: CGFloat?) {
        let strokeLayer = CAShapeLayer()
        strokeLayer.strokeColor = borderColor.cgColor
        strokeLayer.fillColor = nil
        strokeLayer.lineWidth = borderWidth
        if let dashPattern = lineDashPattern {
            strokeLayer.lineDashPattern = dashPattern as [NSNumber]?
        }
        if cornerRadius != nil {
            strokeLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius!).cgPath
        } else {
            strokeLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: self.cornerRadius()).cgPath
        }
        
        strokeLayer.frame = bounds
        layer.addSublayer(strokeLayer)
    }
    
    // MARK: Masks
    
    func addGradientMaskAtTopOfView(percentageOfView: CGFloat) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.white.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0, y: percentageOfView)
        //gradient.locations = [NSNumber(float: 0.5)]
        self.layer.mask = gradient
    }
    
    // MARK: Rounded Corners
    
    func cornerRadius() -> CGFloat { return layer.cornerRadius }
    func cornerRadius(cornerRadius: CGFloat) { layer.cornerRadius = cornerRadius }
    
    func roundCornersToCircle() {
        cornerRadius(cornerRadius: width/2)
    }
    
    func roundCornersToCircle(borderColor: UIColor, borderWidth: CGFloat) {
        roundCornersToCircle()
        self.borderWidth(borderWidth: borderWidth)
        self.borderColor(borderColor: borderColor)
    }
    
    func roundCorners(cornerRadius: CGFloat, borderColor: UIColor?, borderWidth: CGFloat?) {
        self.cornerRadius(cornerRadius: cornerRadius)
        if let borderWidth = borderWidth {
            self.borderWidth(borderWidth: borderWidth)
        }
        if let borderColor = borderColor {
            self.borderColor(borderColor: borderColor)
        }
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect:bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat, inset: UIEdgeInsets) {
        let xInset = inset.right
        let yInset = inset.top
        let newWidth = bounds.width - inset.right - inset.left
        let newHeight = bounds.height - inset.top - inset.bottom
        let rect = CGRect(x: xInset, y: yInset, width: newWidth, height: newHeight)
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }

    // MARK: Shadow
    
    func shadowColor() -> UIColor { return UIColor(cgColor: layer.shadowColor!) }
    func shadowColor(shadowColor: UIColor) { layer.shadowColor = shadowColor.cgColor }
    
    func shadowOffset() -> CGSize { return layer.shadowOffset }
    func shadowOffset(shadowOffset: CGSize) { layer.shadowOffset = shadowOffset }
    
    func shadowOpacity() -> Float { return layer.shadowOpacity }
    func shadowOpacity(shadowOpacity: Float) { layer.shadowOpacity = shadowOpacity }
    
    func shadowRadius() -> CGFloat { return layer.shadowRadius }
    func shadowRadius(shadowRadius: CGFloat) { layer.shadowRadius = shadowRadius }
    
    func shadow(color: UIColor = UIColor.black, offset: CGSize = CGSize(width: 0, height: 0), radius: CGFloat = 6, opacity: Float = 1, isMasked: Bool = false) {
        shadowColor(shadowColor: color)
        shadowOffset(shadowOffset: offset)
        shadowOpacity(shadowOpacity: opacity)
        shadowRadius(shadowRadius: radius)
        if isMasked {
            layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius()).cgPath
            let path = CGMutablePath()
            path.addRect(bounds.insetBy(dx: -10, dy: -10))
            path.addPath(UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius()).cgPath)
            let maskLayer = CAShapeLayer()
            maskLayer.path = path
            maskLayer.fillRule = kCAFillRuleEvenOdd
            layer.mask = maskLayer
        } else {
            layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius()).cgPath
        }
    }
    
    // MARK: AutoLayout fix for animating
    
    func prepForAnimation() {
        removeFromSuperview()
        translatesAutoresizingMaskIntoConstraints = true
        superview?.addSubview(self)
    }
    
    // MARK: Screenshot
    
    func screenshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func screenshot(rect: CGRect) -> UIImage {
        let fullRect = rect.toCGImageRect()
        let fullImage = self.screenshot()
        
        let imageRef = fullImage.cgImage!.cropping(to: fullRect)!
        
        return UIImage(cgImage: imageRef, scale: fullImage.scale, orientation: fullImage.imageOrientation)
    }
    
    func screenshotEverythingAbove(view: UIView) -> UIImage {
        let convertedRect = view.superview!.convert(view.frame, to: self)
        let screenShotRect = CGRect(x: 0, y: 0, width: self.size.width, height: convertedRect.origin.y)
        
        return self.screenshot(rect: screenShotRect)
    }
    
    func screenshotEverythingBelow(view: UIView) -> UIImage {
        let convertedRect = view.superview!.convert(view.frame, to: self)
        let screenShotRect = CGRect(x: 0, y: convertedRect.origin.y + convertedRect.size.height, width: self.size.width, height: self.height - (convertedRect.origin.y + convertedRect.size.height))
        
        if screenShotRect.size.height <= 0 || screenShotRect.size.width <= 0 {
            return UIImage()
        }
        return self.screenshot(rect: screenShotRect)
    }
}

