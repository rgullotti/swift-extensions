//
//  SystemManager.swift
//  AppStuExtensions
//
//  Created by Nick Lemke on 6/1/17.
//
//

import Foundation

//MARK: - Protocol Definition
public protocol SystemManager {
    static var keyWindow: UIWindow? {get}
    static var mainStoryboard: UIStoryboard? {get}
    
    static func initialize()
}

//MARK: - Extensions for Procotol
public extension SystemManager {
    
    static func currentlyVisibleViewController(_ base: UIViewController? = nil) -> UIViewController? {
        let viewController = base ?? keyWindow?.rootViewController
        
        if let nav = viewController as? UINavigationController {
            return currentlyVisibleViewController(nav.visibleViewController)
        }
        else if let tab = viewController as? UITabBarController {
            return currentlyVisibleViewController(tab.selectedViewController)
        }
        else if let presented = viewController?.presentedViewController {
            return currentlyVisibleViewController(presented)
        }
        else {
            return viewController
        }
    }

    static func showAlert(title: String, message: String?, actions: [UIAlertAction]?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let alertActions = actions {
            alertActions.forEach({ (action) in
                alert.addAction(action)
            })
        }
        else {
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                
            }))
        }
        currentlyVisibleViewController()?.present(alert, animated: true, completion: nil)
    }
    
    static func show(viewController: UIViewController, on presentingVC: UIViewController? = nil, animated: Bool = true, forceModal: Bool = false) {
        let onVC = (presentingVC != nil) ? viewController : self.currentlyVisibleViewController()
        if forceModal {
            onVC?.present(viewController, animated: animated, completion: nil)
        }
        else {
            onVC?.show(viewController, sender: onVC)
        }
    }
    
    static func getViewControllerOf<T: UIViewController>(type: T.Type) -> T {
        guard let storyboard = mainStoryboard else {
            fatalError("No reference to a main storyboard")
        }
        return storyboard.instantiateViewController(withIdentifier: String(describing: type)) as! T
    }
    
    static func setRootViewController(to storyboard: UIStoryboard) {
        guard let vc = storyboard.instantiateInitialViewController() else {return}
        let transition = CATransition()
        transition.type = kCATransitionFade
        keyWindow?.set(rootViewController: vc, withTransition: transition)
    }

}
