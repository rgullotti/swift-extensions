//
//  RG+SwiftExtensions.swift
//
//
//  Created by Ryan Gullotti on 10/1/14.
//
//

import CoreFoundation
import Foundation
#if os(iOS)
import UIKit
import QuartzCore
#elseif os(OSX)
    
#else
import UIKit
import WatchKit
#endif

func delay(delay:Double, completion:@escaping ()->()) {
    let deadlineTime = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
        completion()
    }
}

private struct AssociatedKeys {
    static var currentKeyboardHeight = "currentKeyboardHeight"
}

/*****************************************************/
//MARK: - Array Extensions
/*****************************************************/

extension Array where Element: Equatable {
    mutating func appendIfUnique(element: Element) {
        if !contains(element) {
            append(element)
        }
    }
    
    mutating func remove(element: Element) {
        if let index = index(of: element) {
            remove(at: index)
        }
    }
}

/******************************************************
 End Array Extensions
 ******************************************************/

/*****************************************************/
//MARK: - Dictionary Extensions
/*****************************************************/

func + <K, V> (left: [K : V], right: [K : V]) -> [K : V] {
    var newLeft = left
    for (k, v) in right {
        newLeft.updateValue(v, forKey: k)
    }
    return newLeft
}

/*****************************************************
 End Dictionary Extensions
*****************************************************/

#if os(OSX)
    // mac only extensions
    
#else
    // non mac extensions
    
    /*****************************************************/
    //MARK: - UIColor Extensions
    /*****************************************************/
    
    extension UIColor {
        convenience init(hexString: String) {
            let rHex = hexString.substring(with: hexString.index(hexString.startIndex, offsetBy: 0)..<hexString.index(hexString.endIndex, offsetBy: -4))
            let gHex = hexString.substring(with: hexString.index(hexString.startIndex, offsetBy: 2)..<hexString.index(hexString.endIndex, offsetBy: -2))
            let bHex = hexString.substring(with: hexString.index(hexString.startIndex, offsetBy: 4)..<hexString.index(hexString.endIndex, offsetBy: 0))
            self.init(red: UInt(rHex, radix: 16)!, green: UInt(gHex, radix: 16)!, blue: UInt(bHex, radix: 16)!)
        }
        
        convenience init(red: UInt, green: UInt, blue: UInt) {
            assert(red >= 0 && red <= 255, "Invalid red component")
            assert(green >= 0 && green <= 255, "Invalid green component")
            assert(blue >= 0 && blue <= 255, "Invalid blue component")
            
            self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
        }
    }
    
    func getColorThatShowsOnTopOf(color: UIColor) -> UIColor {
        var returnColor = UIColor.black
        
        let componentColors = color.cgColor.components
        
        let redColor = Double((componentColors?[0])!) * 299
        let greenColor = Double((componentColors?[1])!) * 587
        let blueColor = Double((componentColors?[2])!) * 114
        let colorBrightness = (redColor + greenColor + blueColor) / 1000
        if (colorBrightness < 0.5)
        {
            // Color is dark
            returnColor = UIColor.white
        }
        else
        {
            // Color is light
            returnColor = UIColor.black
        }
        
        return returnColor
    }
    
    /******************************************************
     End UIColor Extensions
     ******************************************************/
    
    /*****************************************************/
    //MARK: - UIImage Extensions
    /*****************************************************/
    
    extension UIImage{
        func croppedImage(bound : CGRect) -> UIImage
        {
            let scaledBounds = CGRect(x: bound.origin.x * self.scale, y: bound.origin.y * self.scale, width: bound.size.width * self.scale, height: bound.size.height * self.scale)
            let imageRef = self.cgImage!.cropping(to: scaledBounds)
            let croppedImage : UIImage = UIImage(cgImage: imageRef!, scale: self.scale, orientation: UIImageOrientation.up)
            return croppedImage;
        }
    }
    
    /******************************************************
     End UIImage Extensions
     ******************************************************/
    
    /*****************************************************/
    //MARK: - UIStackView Extensions
    /*****************************************************/
    
    extension UIStackView {
        func addSpacingView(spacing: CGFloat) {
            let spacingView = UIView()
            if axis == .vertical {
                spacingView.anchor().height(constant: spacing).activate()
            } else {
                spacingView.anchor().width(constant: spacing).activate()
            }
            self.addArrangedSubview(spacingView)
        }
        
    }
    
    /******************************************************
     End UIStackView Extensions
     ******************************************************/
    
#endif

#if os(iOS)
    
    /*****************************************************/
    //MARK: - CGFloat Extensions
    /*****************************************************/
    
    extension CGFloat{
        func roundUp() -> CGFloat
        {
            return CGFloat(ceil(Double(self)))
        }
        
        func roundDown() -> CGFloat
        {
            return self.roundUp() - 1
        }
        
        static func onePixel() -> CGFloat
        {
            return 1.0 / UIScreen.main.scale
        }
        
    }
    
    /******************************************************
     End CGFloat Extensions
     ******************************************************/
    
    /*****************************************************/
    //MARK: - Easier Reusable Cells Extensions
    /*****************************************************/
    
    protocol ReusableView: class {
        static var defaultReuseIdentifier: String { get }
    }
    
    extension ReusableView where Self: UIView {
        static var defaultReuseIdentifier: String {
            return String(describing: self)
        }
    }
    
    extension UICollectionViewCell: ReusableView {
    }
    
    extension UITableViewCell: ReusableView {
    }
    
    protocol NibLoadableView: class {
        static var nibName: String { get }
    }
        
    extension NibLoadableView where Self: UIView {
        static var nibName: String {
            return String(describing: self)
        }
    }
    
    extension UIViewController {
        static var classNameAsSwiftString: String {
            return String(describing: self)
        }
    }
    
    extension UICollectionViewCell: NibLoadableView {
    }
    
    extension UITableViewCell: NibLoadableView {
    }
    
    extension UICollectionView {
        func register<T: UICollectionViewCell>(cellClass: T.Type) where T: ReusableView {
            print("register without nib")
            register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
        }

        func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
            print("register with nib\n")
            let bundle = Bundle(for: T.self)
            let nib = UINib(nibName: T.nibName, bundle: bundle)
            register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
        }
        
        func register<T: UICollectionViewCell>(classes: [T.Type]) where T: ReusableView, T: NibLoadableView {
            for className in classes {
                let bundle = Bundle(for: className.self)
                let nib = UINib(nibName: className.nibName, bundle: bundle)
                register(nib, forCellWithReuseIdentifier: className.defaultReuseIdentifier)
            }
        }
        
        func dequeueReusableCellOfClass<T: UICollectionViewCell>(className: T.Type, forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
            guard let cell = dequeueReusableCell(withReuseIdentifier: className.defaultReuseIdentifier, for: indexPath as IndexPath) as? T else {
                fatalError("Could not dequeue cell with identifier: \(className.defaultReuseIdentifier)")
            }
            return cell
        }
    }
    
    extension UITableView {
//        func register<T: UITableViewCell>(cellClass: T.Type) where T: ReusableView  {
//            register(T.self, forCellReuseIdentifier: T.defaultReuseIdentifier)
//        }

        func register<T: UITableViewCell>(cellClass: T.Type) where T: ReusableView, T: NibLoadableView {
            let bundle = Bundle(for: T.self)
            let nib = UINib(nibName: T.nibName, bundle: bundle)
            register(nib, forCellReuseIdentifier: T.defaultReuseIdentifier)
        }

        func register<T: UITableViewCell>(classes: [T.Type]) where T: ReusableView, T: NibLoadableView {
            for className in classes {
                let bundle = Bundle(for: className.self)
                let nib = UINib(nibName: className.nibName, bundle: bundle)
                register(nib, forCellReuseIdentifier: className.defaultReuseIdentifier)
            }
        }
        
        func dequeueReusableCellOfClass<T: UITableViewCell>(className: T.Type, forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
            guard let cell = dequeueReusableCell(withIdentifier: className.defaultReuseIdentifier, for: indexPath as IndexPath) as? T else {
                fatalError("Could not dequeue cell with identifier: \(className.defaultReuseIdentifier)")
            }
            return cell
        }
    }
    
    
    /******************************************************
     End Easier Reusable Cells Extensions
     ******************************************************/
    
    /*****************************************************/
    //MARK: - UICollectionView Extensions
    /*****************************************************/
    
    extension UICollectionView {
        var collectionViewUsableWidth: CGFloat {
            get {
                if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
                    let inset = flowLayout.sectionInset
                    return frame.width - inset.right - inset.left
                } else {
                    return frame.width
                }
            }
        }
    }
    
    /******************************************************
     End UICollectionView Extensions
     ******************************************************/
    
    /*****************************************************/
    //MARK: - UIViewController Extensions
    /*****************************************************/
    
    extension UIAlertController {
        func show() {
            present(animated: true, completion: nil)
        }
        
        func present(animated: Bool, completion: (() -> Void)?) {
            if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
                presentFromController(controller: rootVC, animated: animated, completion: completion)
            }
        }
        
        private func presentFromController(controller: UIViewController, animated: Bool, completion: (() -> Void)?) {
            if  let navVC = controller as? UINavigationController,
                let visibleVC = navVC.visibleViewController {
                presentFromController(controller: visibleVC, animated: animated, completion: completion)
            } else {
                if  let tabVC = controller as? UITabBarController,
                    let selectedVC = tabVC.selectedViewController {
                    presentFromController(controller: selectedVC, animated: animated, completion: completion)
                } else {
                    controller.present(self, animated: animated, completion: completion)
                }
            }
        }
        
        class func showAlert(title: String, message: String, actions: [UIAlertAction]?) {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            if let alertActions = actions {
                for action in alertActions {
                    alert.addAction(action)
                }
            } else {
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    
                }))
            }
            alert.show()
        }
    }

    extension UIViewController {
        func showAlert(title: String, message: String, actions: [UIAlertAction]?) {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            if let alertActions = actions {
                for action in alertActions {
                    alert.addAction(action)
                }
            } else {
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    
                }))
            }
            present(alert, animated: true, completion: nil)
        }
        
        var currentKeyboardHeight: CGFloat! {
            get {
                if objc_getAssociatedObject(self, &AssociatedKeys.currentKeyboardHeight) == nil {
                    objc_setAssociatedObject(self, &AssociatedKeys.currentKeyboardHeight, NSNumber(value: 0.0), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                }
                
                return CGFloat((objc_getAssociatedObject(self, &AssociatedKeys.currentKeyboardHeight) as! NSNumber).floatValue)
            }
            set(newValue) {
                objc_setAssociatedObject(self, &AssociatedKeys.currentKeyboardHeight, NSNumber(value: Float(newValue)), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        
        func adjustScrollViewForKeyboardDisplay(scrollView: UIScrollView) {
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardDidShow, object: nil, queue: nil, using: { (notification) in
                guard let info = notification.userInfo, let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size else { return }
                
                self.currentKeyboardHeight = keyboardSize.height
                scrollView.contentInset.bottom = keyboardSize.height
                scrollView.scrollIndicatorInsets.bottom = keyboardSize.height
            })
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardDidHide, object: nil, queue: nil, using: { (notification) in
                self.currentKeyboardHeight = 0.0
                scrollView.contentInset.bottom = 0.0
                scrollView.scrollIndicatorInsets.bottom = 0.0
            })
        }
    }
    
    /******************************************************
     End UIViewController Extensions
     ******************************************************/
    
    /*****************************************************/
    //MARK: - CGRect Extensions
    /*****************************************************/
    
    extension CGRect {
        func toCGImageRect() -> CGRect {
            let scale = UIScreen.main.scale
            return CGRect(x: self.origin.x * scale, y: self.origin.y * scale, width: self.size.width * scale, height: self.size.height * scale)
        }
    }
    
    /******************************************************
     End CGRect Extensions
     ******************************************************/
    
    /*****************************************************/
    //MARK: - UIImageView Extensions
    /*****************************************************/
    
    extension UIImageView{
        func roundImage()
        {
            //height and width should be the same
            self.clipsToBounds = true
            self.layer.cornerRadius = self.frame.size.width / 2;
        }
    }
    
    /******************************************************
     End UIImageView Extensions
     ******************************************************/
    
    /*****************************************************/
    //MARK: - UIView Extensions
    /*****************************************************/
    
    //MARK: Separator Extensions
    
    public extension UIView {
        
        public func addHorizontalSeparatorLine(to toSide: NSLayoutAnchor<AnyObject>, leftMargin left: CGFloat = 0.0, rightMargin right: CGFloat = 0.0, color: UIColor = UIColor.lightGray) -> UIView {
            return addSeparatorLine(to: toSide, leftMargin: left, rightMargin: right, color: color)
        }
        
        public func addVerticalSeparatorLine(to toSide: NSLayoutAnchor<AnyObject>, topMargin left: CGFloat = 0.0, bottomMargin right: CGFloat = 0.0, color: UIColor = UIColor.lightGray) -> UIView {
            return addSeparatorLine(to: toSide, leftMargin: left, rightMargin: right, color: color)
        }
        
        private func addSeparatorLine(to toSide: NSLayoutAnchor<AnyObject>, leftMargin left: CGFloat = 0.0, rightMargin right: CGFloat = 0.0, color: UIColor = UIColor.lightGray) -> UIView {
            let separatorLine = UIView()
            separatorLine.backgroundColor = color
            separatorLine.alpha = 0.5
            addSubview(separatorLine)
            
            if toSide == topAnchor {
                separatorLine.anchor().leftToSuperview(constant: left).rightToSuperview(constant: right).topToSuperview().height(constant: CGFloat.onePixel()).activate()
                
            } else if toSide == bottomAnchor {
                separatorLine.anchor().leftToSuperview(constant: left).rightToSuperview(constant: right).bottomToSuperview().height(constant: CGFloat.onePixel()).activate()
                
            } else if toSide == leftAnchor {
                separatorLine.anchor().topToSuperview(constant: left).bottomToSuperview(constant: right).leftToSuperview().width(constant: CGFloat.onePixel()).activate()
                
            } else if toSide == rightAnchor {
                separatorLine.anchor().topToSuperview(constant: left).bottomToSuperview(constant: right).rightToSuperview().width(constant: CGFloat.onePixel()).activate()
                
            }
            
            return separatorLine
        }
    }
    
    //MARK: Layout Extensions
    
    public extension UIView {
        public func anchor() -> Anchor { return Anchor(view: self) }
    }
    
    public struct Anchor {
        public let view: UIView
        public let top: NSLayoutConstraint?
        public let left: NSLayoutConstraint?
        public let bottom: NSLayoutConstraint?
        public let right: NSLayoutConstraint?
        public let height: NSLayoutConstraint?
        public let width: NSLayoutConstraint?
        public let centerX: NSLayoutConstraint?
        public let centerY: NSLayoutConstraint?
        
        internal init(view: UIView) {
            self.view = view
            top = nil
            left = nil
            bottom = nil
            right = nil
            height = nil
            width = nil
            centerX = nil
            centerY = nil
        }
        
        private init(view: UIView,
                     top: NSLayoutConstraint?,
                     left: NSLayoutConstraint?,
                     bottom: NSLayoutConstraint?,
                     right: NSLayoutConstraint?,
                     height: NSLayoutConstraint?,
                     width: NSLayoutConstraint?,
                     centerX: NSLayoutConstraint?,
                     centerY: NSLayoutConstraint?)
        {
            self.view = view
            self.top = top
            self.left = left
            self.bottom = bottom
            self.right = right
            self.height = height
            self.width = width
            self.centerX = centerX
            self.centerY = centerY
        }
        
        private func update(edge: NSLayoutAttribute, constraint: NSLayoutConstraint?) -> Anchor {
            var top = self.top
            var left = self.left
            var bottom = self.bottom
            var right = self.right
            var height = self.height
            var width = self.width
            var centerX = self.centerX
            var centerY = self.centerY
            
            switch edge {
            case .top: top = constraint
            case .left: left = constraint
            case .bottom: bottom = constraint
            case .right: right = constraint
            case .height: height = constraint
            case .width: width = constraint
            case .centerX: centerX = constraint
            case .centerY: centerY = constraint
            default: return self
            }
            
            return Anchor(
                view: self.view,
                top: top,
                left: left,
                bottom: bottom,
                right: right,
                height: height,
                width: width,
                centerX: centerX,
                centerY: centerY)
        }
        
        private func insert(anchor: Anchor) -> Anchor {
            return Anchor(
                view: self.view,
                top: anchor.top ?? top,
                left: anchor.left ?? left,
                bottom: anchor.bottom ?? bottom,
                right: anchor.right ?? right,
                height: anchor.height ?? height,
                width: anchor.width ?? width,
                centerX: anchor.centerX ?? centerX,
                centerY: anchor.centerY ?? centerY)
        }
        
        //MARK: Hugging
        public func hugContentWidth() -> Anchor {
            view.setContentHuggingPriority(UILayoutPriorityRequired, for: .horizontal)
            return self
        }
        
        public func hugContentHeight() -> Anchor {
            view.setContentHuggingPriority(UILayoutPriorityRequired, for: .vertical)
            return self
        }
        
        // MARK: Anchor to superview edges
        public func topToSuperview(constant c: CGFloat = 0) -> Anchor {
            guard let superview = view.superview else {
                return self
            }
			
            return top(to: superview.topAnchor, constant: c)
        }
        
        public func leftToSuperview(constant c: CGFloat = 0) -> Anchor {
            guard let superview = view.superview else {
                return self
            }
            return left(to: superview.leftAnchor, constant: c)
        }
        
        public func bottomToSuperview(constant c: CGFloat = 0) -> Anchor {
            guard let superview = view.superview else {
                return self
            }
            return bottom(to: superview.bottomAnchor, constant: c)
        }
        
        public func rightToSuperview(constant c: CGFloat = 0) -> Anchor {
            guard let superview = view.superview else {
                return self
            }
            return right(to: superview.rightAnchor, constant: c)
        }
        
        public func edgesToSuperview(omitEdge e: NSLayoutAttribute = .notAnAttribute, insets: UIEdgeInsets = UIEdgeInsets.zero) -> Anchor {
            let superviewAnchors = topToSuperview(constant: insets.top)
                .leftToSuperview(constant: insets.left)
                .bottomToSuperview(constant: insets.bottom)
                .rightToSuperview(constant: insets.right)
                .update(edge: e, constraint: nil)
            return self.insert(anchor: superviewAnchors)
        }
        
        // MARK: Anchor to superview axises
        public func centerXToSuperview() -> Anchor {
            guard let superview = view.superview else {
                return self
            }
            return centerX(to: superview.centerXAnchor)
        }
        
        public func centerYToSuperview() -> Anchor {
            guard let superview = view.superview else {
                return self
            }
            return centerY(to: superview.centerYAnchor)
        }
        
        public func centerToSuperview() -> Anchor {
            guard let superview = view.superview else {
                return self
            }
            return centerX(to: superview.centerXAnchor)
                .centerY(to: superview.centerYAnchor)
        }
        
        // MARK: Anchor to
        public func top(to anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .top, constraint: view.topAnchor.constraint(equalTo: anchor, constant: c))
        }
        
        public func left(to anchor: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .left, constraint: view.leftAnchor.constraint(equalTo: anchor, constant: c))
        }
        
        public func bottom(to anchor: NSLayoutAnchor<NSLayoutYAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .bottom, constraint: view.bottomAnchor.constraint(equalTo: anchor, constant: c))
        }
        
        public func right(to anchor: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .right, constraint: view.rightAnchor.constraint(equalTo: anchor, constant: c))
        }
        
        // MARK: Anchor greaterOrEqual
        public func top(greaterOrEqual anchor: NSLayoutAnchor<AnyObject>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .top, constraint: view.topAnchor.constraint(greaterThanOrEqualTo: anchor as! NSLayoutAnchor<NSLayoutYAxisAnchor>, constant: c))
        }
        
        public func left(greaterOrEqual anchor: NSLayoutAnchor<AnyObject>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .left, constraint: view.leftAnchor.constraint(greaterThanOrEqualTo: anchor as! NSLayoutAnchor<NSLayoutXAxisAnchor>, constant: c))
        }
        
        public func bottom(greaterOrEqual anchor: NSLayoutAnchor<AnyObject>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .bottom, constraint: view.bottomAnchor.constraint(greaterThanOrEqualTo: anchor as! NSLayoutAnchor<NSLayoutYAxisAnchor>, constant: c))
        }
        
        public func right(greaterOrEqual anchor: NSLayoutAnchor<AnyObject>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .right, constraint: view.rightAnchor.constraint(greaterThanOrEqualTo: anchor as! NSLayoutAnchor<NSLayoutXAxisAnchor>, constant: c))
        }
        
        // MARK: Anchor lessOrEqual
        public func top(lesserOrEqual anchor: NSLayoutAnchor<AnyObject>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .top, constraint: view.topAnchor.constraint(lessThanOrEqualTo: anchor as! NSLayoutAnchor<NSLayoutYAxisAnchor>, constant: c))
        }
        
        public func left(lesserOrEqual anchor: NSLayoutAnchor<AnyObject>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .left, constraint: view.leftAnchor.constraint(lessThanOrEqualTo: anchor as! NSLayoutAnchor<NSLayoutXAxisAnchor>, constant: c))
        }
        
        public func bottom(lesserOrEqual anchor: NSLayoutAnchor<AnyObject>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .bottom, constraint: view.bottomAnchor.constraint(lessThanOrEqualTo: anchor as! NSLayoutAnchor<NSLayoutYAxisAnchor>, constant: c))
        }
        
        public func right(lesserOrEqual anchor: NSLayoutAnchor<AnyObject>, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .right, constraint: view.rightAnchor.constraint(lessThanOrEqualTo: anchor as! NSLayoutAnchor<NSLayoutXAxisAnchor>, constant: c))
        }
        
        // MARK: Dimension anchors
        public func height(constant c: CGFloat) -> Anchor {
            return update(edge: .height, constraint: view.heightAnchor.constraint(equalToConstant: c))
        }
        
        public func height(to dimension: NSLayoutDimension, multiplier m: CGFloat = 1) -> Anchor {
            return update(edge: .height, constraint: view.heightAnchor.constraint(equalTo: dimension, multiplier: m))
        }
        
        public func width(constant c: CGFloat) -> Anchor {
            return update(edge: .width, constraint: view.widthAnchor.constraint(equalToConstant: c))
        }
        
        public func width(to dimension: NSLayoutDimension, multiplier m: CGFloat = 1) -> Anchor {
            return update(edge: .width, constraint: view.widthAnchor.constraint(equalTo: dimension, multiplier: m))
        }
        
        // MARK: Axis anchors
        public func centerX(to axis: NSLayoutXAxisAnchor, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .centerX, constraint: view.centerXAnchor.constraint(equalTo: axis, constant: c))
        }
        
        public func centerY(to axis: NSLayoutYAxisAnchor, constant c: CGFloat = 0) -> Anchor {
            return update(edge: .centerY, constraint: view.centerYAnchor.constraint(equalTo: axis, constant: c))
        }
        
        // MARK: Activation
        @discardableResult public func activate() -> Anchor {
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = [top, left, bottom, right, height, width, centerX, centerY].flatMap({ $0 })
            NSLayoutConstraint.activate(constraints)
            return self
        }
    }
    
    //MARK: Helpers
    
    extension UIView{
        
        class func loadFromNibNamed(nibNamed: String, bundle : Bundle? = nil) -> UIView? {
            return UINib(nibName: nibNamed, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView
        }
        
        // MARK: Variables
        var width:      CGFloat { return self.frame.size.width }
        var height:     CGFloat { return self.frame.size.height }
        var size:       CGSize  { return self.frame.size}
        
        var origin:     CGPoint { return self.frame.origin }
        var centerX:    CGFloat { return self.center.x }
        var centerY:    CGFloat { return self.center.y }
        
        var left:       CGFloat { return self.frame.origin.x }
        var right:      CGFloat { return self.frame.origin.x + self.frame.size.width }
        var top:        CGFloat { return self.frame.origin.y }
        var bottom:     CGFloat { return self.frame.origin.y + self.frame.size.height }
        
        // MARK: Functions
        // MARK: Positioning and Size
        
        func setWidth(width:CGFloat)
        {
            self.frame.size.width = width
        }
        
        func setHeight(height:CGFloat)
        {
            self.frame.size.height = height
        }
        
        func setSize(size:CGSize)
        {
            self.frame.size = size
        }
        
        func setOrigin(point:CGPoint)
        {
            self.frame.origin = point
        }
        
        func setX(x:CGFloat) //only change the origin x
        {
            self.frame.origin = CGPoint(x: x, y: self.frame.origin.y)
        }
        
        func setY(y:CGFloat) //only change the origin x
        {
            self.frame.origin = CGPoint(x: self.frame.origin.x, y: y)
        }
        
        func setCenterX(x:CGFloat) //only change the origin x
        {
            self.center = CGPoint(x: x, y: self.center.y)
        }
        
        func setCenterY(y:CGFloat) //only change the origin x
        {
            self.center = CGPoint(x: self.center.x, y: y)
        }
        
        func roundCorner(radius:CGFloat)
        {
            self.layer.cornerRadius = radius
        }
        
        func setTop(top:CGFloat)
        {
            self.frame.origin.y = top
        }
        
        func setLeft(left:CGFloat)
        {
            self.frame.origin.x = left
        }
        
        func setRight(right:CGFloat)
        {
            self.frame.origin.x = right - self.frame.size.width
        }
        
        func setBottom(bottom:CGFloat)
        {
            self.frame.origin.y = bottom - self.frame.size.height
        }
        
        func centerInSuperview()
        {
            if superview == nil {
                return
            }
            center = CGPoint(x: superview!.centerX, y: superview!.centerY)
        }
        
        func centerHorizontally()
        {
            if superview == nil {
                return
            }
            center = CGPoint(x: superview!.centerX, y: origin.y)
        }
        
        func centerVertically()
        {
            if superview == nil {
                return
            }
            center = CGPoint(x: origin.x, y: superview!.centerY)
        }
        
        //MARK: Calculation Functions
        func percentageOfPan(offset: CGFloat) -> CGFloat {
            var percentage = offset / self.width
            
            if percentage < -1.0 {
                percentage = -1.0
            } else if percentage > 1.0 {
                percentage = 1.0
            }
            
            if percentage < 0 {
                percentage = percentage * -1
            }
            
            return percentage
        }
        
        // MARK: Border
        
        func borderColor() -> UIColor { return UIColor(cgColor: layer.borderColor!) }
        func borderColor(borderColor: UIColor) { layer.borderColor = borderColor.cgColor }
        
        func borderWidth() -> CGFloat { return layer.borderWidth }
        func borderWidth(borderWidth: CGFloat) { layer.borderWidth = borderWidth }
        
        func borderWithDashPattern(lineDashPattern: [Int]?, borderWidth: CGFloat, borderColor: UIColor, cornerRadius: CGFloat?)
        {
            let strokeLayer = CAShapeLayer()
            strokeLayer.strokeColor = borderColor.cgColor
            strokeLayer.fillColor = nil
            strokeLayer.lineWidth = borderWidth
            if let dashPattern = lineDashPattern {
                strokeLayer.lineDashPattern = dashPattern as [NSNumber]?
            }
            if cornerRadius != nil {
                strokeLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius!).cgPath
            } else {
                strokeLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: self.cornerRadius()).cgPath
            }
            
            strokeLayer.frame = bounds
            layer.addSublayer(strokeLayer)
        }
        
        // MARK: Masks
        
        func addGradientMaskAtTopOfView(percentageOfView: CGFloat) {
            let gradient = CAGradientLayer()
            gradient.frame = self.bounds
            gradient.colors = [UIColor.clear.cgColor, UIColor.white.cgColor]
            gradient.startPoint = CGPoint(x: 0, y: 0.0)
            gradient.endPoint = CGPoint(x: 0, y: percentageOfView)
            //gradient.locations = [NSNumber(float: 0.5)]
            self.layer.mask = gradient
        }
        
        // MARK: Rounded Corners
        
        func cornerRadius() -> CGFloat { return layer.cornerRadius }
        func cornerRadius(cornerRadius: CGFloat) { layer.cornerRadius = cornerRadius }
        
        func roundCornersToCircle()
        {
            cornerRadius(cornerRadius: width/2)
        }
        func roundCornersToCircle(borderColor: UIColor, borderWidth: CGFloat)
        {
            roundCornersToCircle()
            self.borderWidth(borderWidth: borderWidth)
            self.borderColor(borderColor: borderColor)
        }
        
        func roundCorners(cornerRadius: CGFloat, borderColor: UIColor?, borderWidth: CGFloat?)
        {
            self.cornerRadius(cornerRadius: cornerRadius)
            if let borderWidth = borderWidth {
                self.borderWidth(borderWidth: borderWidth)
            }
            if let borderColor = borderColor {
                self.borderColor(borderColor: borderColor)
            }
        }
        
        func roundCorners(corners: UIRectCorner, radius: CGFloat) {
            let path = UIBezierPath(roundedRect:bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            layer.mask = maskLayer
        }
        
        // MARK: Shadow
        
        func shadowColor() -> UIColor { return UIColor(cgColor: layer.shadowColor!) }
        func shadowColor(shadowColor: UIColor) { layer.shadowColor = shadowColor.cgColor }
        
        func shadowOffset() -> CGSize { return layer.shadowOffset }
        func shadowOffset(shadowOffset: CGSize) { layer.shadowOffset = shadowOffset }
        
        func shadowOpacity() -> Float { return layer.shadowOpacity }
        func shadowOpacity(shadowOpacity: Float) { layer.shadowOpacity = shadowOpacity }
        
        func shadowRadius() -> CGFloat { return layer.shadowRadius }
        func shadowRadius(shadowRadius: CGFloat) { layer.shadowRadius = shadowRadius }
        
        func shadow(color: UIColor = UIColor.black, offset: CGSize = CGSize(width: 0, height: 0), radius: CGFloat = 6, opacity: Float = 1, isMasked: Bool = false)
        {
            shadowColor(shadowColor: color)
            shadowOffset(shadowOffset: offset)
            shadowOpacity(shadowOpacity: opacity)
            shadowRadius(shadowRadius: radius)
            if isMasked {
                layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius()).cgPath
                let path = CGMutablePath()
                path.addRect(bounds.insetBy(dx: -10, dy: -10))
                path.addPath(UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius()).cgPath)
                let maskLayer = CAShapeLayer()
                maskLayer.path = path
                maskLayer.fillRule = kCAFillRuleEvenOdd
                layer.mask = maskLayer
            } else {
                layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius()).cgPath
            }
        }
        
        // MARK: AutoLayout fix for animating
        
        func prepForAnimation()
        {
            removeFromSuperview()
            translatesAutoresizingMaskIntoConstraints = true
            superview?.addSubview(self)
        }
        
        // MARK: Screenshot
        
        func screenshot() -> UIImage {
            UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
            
            drawHierarchy(in: self.bounds, afterScreenUpdates: true)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image!
        }
        
        func screenshot(rect: CGRect) -> UIImage {
            let fullRect = rect.toCGImageRect()
            let fullImage = self.screenshot()
            
            let imageRef = fullImage.cgImage!.cropping(to: fullRect)!
            
            return UIImage(cgImage: imageRef, scale: fullImage.scale, orientation: fullImage.imageOrientation)
        }
        
        func screenshotEverythingAbove(view: UIView) -> UIImage {
            let convertedRect = view.superview!.convert(view.frame, to: self)
            let screenShotRect = CGRect(x: 0, y: 0, width: self.size.width, height: convertedRect.origin.y)
            
            return self.screenshot(rect: screenShotRect)
        }
        
        func screenshotEverythingBelow(view: UIView) -> UIImage {
            let convertedRect = view.superview!.convert(view.frame, to: self)
            let screenShotRect = CGRect(x: 0, y: convertedRect.origin.y + convertedRect.size.height, width: self.size.width, height: self.height - (convertedRect.origin.y + convertedRect.size.height))
            
            if screenShotRect.size.height <= 0 || screenShotRect.size.width <= 0 {
                return UIImage()
            }
            return self.screenshot(rect: screenShotRect)
        }
    }
    
    /******************************************************
     End UIView Extensions
     ******************************************************/
    
    #elseif os(OSX)
    // mac extensions
    
    #else
    
    // Watch OS
    enum WatchResolution {
        case Watch38mm, Watch42mm, Unknown
    }
    
    extension WKInterfaceDevice {
        class func currentResolution() -> WatchResolution {
            let watch38mmRect = CGRect(x: 0, y: 0, width: 136, height: 170)
            let watch42mmRect = CGRect(x: 0, y: 0, width: 156, height: 195)
            
            let currentBounds = WKInterfaceDevice.currentDevice().screenBounds
            
            switch currentBounds {
            case watch38mmRect:
                return .Watch38mm
            case watch42mmRect:
                return .Watch42mm
            default:
                return .Unknown
            }
        }
    }
    
#endif

/*****************************************************/
//MARK: - Int Extensions
/*****************************************************/

extension Int{
    var isEven:Bool             {return (self % 2 == 0)}
    var isOdd:Bool              {return (self % 2 != 0)}
    var isPositive:Bool         {return (self >= 0)}
    var isNegative:Bool         {return (self < 0)}
    var toDouble:Double         {return Double(self)}
    var toFloat:Float           {return Float(self)}
    var toCGFloat:CGFloat       {return CGFloat(self)}
    var pixelsToPoints:CGFloat  {return CGFloat(self) / 2}
    
    var digits:Int {//this only works in bound of LONG_MAX 2147483647, the maximum value of int
        if(self == 0)
        {
            return 1
        }
        else if(Int(fabs(Double(self))) <= LONG_MAX)
        {
            return Int(log10(fabs(Double(self)))) + 1
        }
        else
        {
            return -1; //out of bound
        }
    }
    
    func ordinalIndicatorString() -> String {
        switch self {
        case 1, 21, 31:
            return "\(self)st"
            
        case 2, 22:
            return "\(self)nd"
            
        case 3, 23:
            return "\(self)rd"
            
        default:
            return "\(self)th"
        }
    }
}

/******************************************************
End Int Extensions
******************************************************/

/*****************************************************/
//MARK: - Double Extensions
/*****************************************************/

extension Double{
    func roundToDecimalDigits(decimals:Int) -> Double
    {
        let a : Double = self
        let format : NumberFormatter = NumberFormatter()
        format.numberStyle = NumberFormatter.Style.decimal
        format.roundingMode = NumberFormatter.RoundingMode.halfUp
        format.maximumFractionDigits = 2
        let string: NSString = format.string(from: NSNumber(value: a))! as NSString
        print(string.doubleValue)
        return string.doubleValue
    }
}

/******************************************************
End Double Extensions
******************************************************/

/*****************************************************/
//MARK: - String Extensions
/*****************************************************/

extension String {
    var isNotEmpty: Bool {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
    }
    
    func containsString(s:String) -> Bool
    {
        if(self.range(of: s) != nil)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func containsString(s:String, compareOption: NSString.CompareOptions) -> Bool
    {
        if((self.range(of: s, options: compareOption)) != nil)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    var length: Int {
        return self.characters.count
    }
    
    var objcLength: Int {
        return self.utf16.count
    }
    
    func makePluralIfNecessary() -> String {
        if characters.last != "s" {
            return self + "s"
        } else {
            return self
        }
    }
    
    func makeSingularIfNecessary() -> String {
        if characters.last == "s" {
            return String(characters.dropLast())
        } else {
            return self
        }
    }
    
    //MARK: Linguistics
    
    /**
    Returns the langauge of a String
    
    NOTE: String has to be at least 4 characters, otherwise the method will return nil.
    
    - returns: String! Returns a string representing the langague of the string (e.g. en, fr, or und for undefined).
    */
    func detectLanguage() -> String! {
        if self.length > 4 {
            let tagger = {
                return NSLinguisticTagger(tagSchemes: [NSLinguisticTagSchemeLanguage], options: 0)
            }()
            tagger.string = self
            return tagger.tag(at: 0, scheme: NSLinguisticTagSchemeLanguage, tokenRange: nil, sentenceRange: nil)
        }
        return nil
    }
    
    /**
    Returns the script of a String
    
    - returns: String! returns a string representing the script of the String (e.g. Latn, Hans).
    */
    func detectScript() -> String! {
        let tagger = {
            return NSLinguisticTagger(tagSchemes: [NSLinguisticTagSchemeScript], options: 0)
        }()
        tagger.string = self
        return tagger.tag(at: 0, scheme: NSLinguisticTagSchemeScript, tokenRange: nil, sentenceRange: nil)
    }
    
    /**
    Check the text direction of a given String.
    
    NOTE: String has to be at least 4 characters, otherwise the method will return false.
    
    - returns: Bool The Bool will return true if the string was writting in a right to left langague (e.g. Arabic, Hebrew)
    
    */
    func isRightToLeft() -> Bool {
        let language = self.detectLanguage()
        return (language == "ar" || language == "he")
    }
    
    
    //MARK: Usablity & Social
    
    /**
    Check that a String is only made of white spaces, and new line characters.
    
    - returns: Bool
    */
    func isOnlyEmptySpacesAndNewLineCharacters() -> Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).length == 0
    }
    
    /**
    Checks if a string is an email address using NSDataDetector.
    
    - returns: Bool
    */
    var isEmail: Bool {
        let dataDetector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue),
        firstMatch = dataDetector.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.length))
        
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    /**
    Gets an array of URLs for all links found in a String
    
    - returns: [URL]
    */
    func getURLs() -> [URL] {
        let detector: NSDataDetector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let links = detector.matches(in: self, options: NSRegularExpression.MatchingOptions.withTransparentBounds, range: NSMakeRange(0, self.utf16.count))
        return links.filter { link in
            return link.url != nil
            }.map { link -> URL in
                return link.url!
        }
    }
    
    
    /**
    Gets an array of dates for all dates found in a String
    
    - returns: [Date]
    */
    func getDates() -> [Date] {
        let detector: NSDataDetector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.date.rawValue)
        let links = detector.matches(in: self, options: NSRegularExpression.MatchingOptions.withTransparentBounds, range: NSMakeRange(0, self.utf16.count))
        
        return links.filter { link in
            return link.date != nil
            }.map { link -> Date in
                return link.date!
        }
    }
    
    /**
    Gets an array of strings (hashtags #acme) for all links found in a String
    
    - returns: [String]
    */
    func getHashtags() -> [String] {
        let hashtagDetector = try! NSRegularExpression(pattern: "#(\\w+)", options: NSRegularExpression.Options.caseInsensitive),
        results = hashtagDetector.matches(in: self, options: NSRegularExpression.MatchingOptions.withoutAnchoringBounds, range: NSMakeRange(0, self.utf16.count)) 
        
        return results.map { textCheckingResult -> String in
            return self[textCheckingResult.rangeAt(0)]
        }
    }
    
    /**
    Gets an array of distinct strings (hashtags #acme) for all hashtags found in a String
    
    - returns: [String]
    */
    func getUniqueHashtags() -> [String] {
        return NSSet(array: self.getHashtags()).allObjects as! [String]
    }
    
    
    /**
    Gets an array of strings (mentions @apple) for all mentions found in a String
    
    - returns: [String]
    */
    func getMentions() -> [String] {
        let mentionDetector = try! NSRegularExpression(pattern: "@(\\w+)", options: NSRegularExpression.Options.caseInsensitive),
        results = mentionDetector.matches(in: self, options: NSRegularExpression.MatchingOptions.withoutAnchoringBounds, range: NSMakeRange(0, self.utf16.count)) 
        
        return results.map { textCheckingResult -> String in
            return self[textCheckingResult.rangeAt(0)]
        }
    }
    
    /**
    Check if a String contains a Date in it.
    
    - returns: Bool with true value if it does
    */
    func contaiDate() -> Bool {
        return self.getDates().count > 0
    }
    
    /**
    - returns: Base64 encoded string
    */
    func encodeToBase64Encoding() -> String {
        let utf8str = self.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
        base64EncodedString = utf8str.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        return base64EncodedString
    }
    
    /**
    - returns: Decoded Base64 string
    */
    func decodeFromBase64Encoding() -> String {
        let base64data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0))!,
        decodedString = String(data: base64data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        return decodedString
    }
    
    /**
    Float value from a string
    */
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    // MARK: Subscript Methods
    
    subscript (i: Int) -> String {
        return String(Array(self.characters)[i])
    }
    
    subscript (r: Range<Int>) -> String {
        let start = self.index(startIndex, offsetBy: r.lowerBound),
        end = self.index(startIndex, offsetBy: r.upperBound)
        
        return substring(with: start ..< end)
    }
    
    subscript (range: NSRange) -> String {
        let end = range.location + range.length
        return self[range.location ..< end]
    }
    
    subscript (substring: String) -> Range<String.Index>? {
        return range(of: substring, options: String.CompareOptions.literal, range: startIndex ..< endIndex, locale: NSLocale.current)
    }
}

/******************************************************
End String Extensions
******************************************************/

/*****************************************************/
//MARK: - Date Extensions
/*****************************************************/

enum DateFormat {
    case ISO8601, DotNet, RSS, AltRSS
    case Custom(String)
}

extension Date {
    func daysInBetweenDate(date: Date) -> Double
    {
        var diff = self.timeIntervalSinceNow - date.timeIntervalSinceNow
        diff = fabs(diff/86400)
        return diff
    }
    
    func daysInBetweenDateSigned(date: Date) -> Double
    {
        var diff = self.timeIntervalSinceNow - date.timeIntervalSinceNow
        diff = diff/86400
        return diff
    }
    
    func hoursInBetweenDate(date: Date) -> Double
    {
        var diff = self.timeIntervalSinceNow - date.timeIntervalSinceNow
        diff = fabs(diff/3600)
        return diff
    }
    
    func minutesInBetweenDate(date: Date) -> Double
    {
        var diff = self.timeIntervalSinceNow - date.timeIntervalSinceNow
        diff = fabs(diff/60)
        return diff
    }
    
    func secondsInBetweenDate(date: Date) -> Double
    {
        var diff = self.timeIntervalSinceNow - date.timeIntervalSinceNow
        diff = fabs(diff)
        return diff
    }
    
    // MARK: Intervals In Seconds
    private static func minuteInSeconds() -> Double { return 60 }
    private static func hourInSeconds() -> Double { return 3600 }
    private static func dayInSeconds() -> Double { return 86400 }
    private static func weekInSeconds() -> Double { return 604800 }
    private static func yearInSeconds() -> Double { return 31556926 }
    
    // MARK: Components
    private static func componentFlags() -> Set<Calendar.Component> { return [.year, .month, .day, .weekOfMonth, .hour, .minute, .second, .weekday, .weekdayOrdinal, .weekOfYear] }
    
    private static func basicComponentFlags() -> Set<Calendar.Component> { return [.year, .month, .day] }
    
    static func components(fromDate: Date) -> DateComponents {
        return Calendar.current.dateComponents(Date.componentFlags(), from: fromDate)
    }
    
    func components() -> DateComponents  {
        return Date.components(fromDate: self)
    }
    
    // MARK: Date From String
    
    init(fromString string: String, format:DateFormat)
    {
        if string.isEmpty {
            self.init()
            return
        }
        
        let string = string as String
        
        switch format {
            
        case .DotNet:
            
            // Expects "/Date(1268123281843)/"
            let startIndex = string.range(of: "(")!.upperBound
            let endIndex = string.range(of: ")")!.lowerBound
            let range = startIndex ..< endIndex
            let milliseconds = (string.substring(with: range) as NSString).longLongValue
            let interval = TimeInterval(milliseconds / 1000)
            self.init(timeIntervalSince1970: interval)
            
        case .ISO8601:
            
            var s = string
            if string.hasSuffix(" 00:00") {
                s = s.substring(to: s.index(s.endIndex, offsetBy: -6)) + "GMT"
            } else if string.hasSuffix("Z") {
                s = s.substring(to: s.index(s.endIndex, offsetBy: -1)) + "GMT"
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
            if let date = formatter.date(from: string) {
                self.init()
                self = Date(timeInterval: 0, since: date)
//                self(timeInterval:0, since:date)
            } else {
                self.init()
            }
            
        case .RSS:
            
            var s  = string
            if string.hasSuffix("Z") {
//                s = s.substringToIndex(s.length-1) + "GMT"
                s = s.substring(to: s.index(s.endIndex, offsetBy: -1)) + "GMT"
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "EEE, d MMM yyyy HH:mm:ss ZZZ"
            if let date = formatter.date(from: string) {
                self.init()
                self = Date(timeInterval: 0, since: date)
//                self(timeInterval:0, since:date)
            } else {
                self.init()
            }
            
        case .AltRSS:
            
            var s  = string
            if string.hasSuffix("Z") {
//                s = s.substringToIndex(s.length-1) + "GMT"
                s = s.substring(to: s.index(s.endIndex, offsetBy: -1)) + "GMT"
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "d MMM yyyy HH:mm:ss ZZZ"
            if let date = formatter.date(from: string) {
                self.init()
                self = Date(timeInterval: 0, since: date)
//                self(timeInterval:0, since:date)
            } else {
                self.init()
            }
            
        case .Custom(let dateFormat):
            
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat
            if let date = formatter.date(from: string) {
                self.init()
                self = Date(timeInterval: 0, since: date)
//                self(timeInterval:0, since:date)
            } else {
                self.init()
            }
        }
    }
    
    
    
    // MARK: Comparing Dates
    
    func isEqualToDateIgnoringTime(date: Date) -> Bool
    {
        let comp1 = Date.components(fromDate: self)
        let comp2 = Date.components(fromDate: date)
        return ((comp1.year == comp2.year) && (comp1.month == comp2.month) && (comp1.day == comp2.day))
    }
    
    func isToday() -> Bool
    {
        return self.isEqualToDateIgnoringTime(date: Date())
    }
    
    func isTomorrow() -> Bool
    {
        return self.isEqualToDateIgnoringTime(date: Date().dateByAddingDays(days: 1))
    }
    
    func isYesterday() -> Bool
    {
        return self.isEqualToDateIgnoringTime(date: Date().dateBySubtractingDays(days:1))
    }
    
    func isSameWeekAsDate(date: Date) -> Bool
    {
        let comp1 = Date.components(fromDate: self)
        let comp2 = Date.components(fromDate: date)
        // Must be same week. 12/31 and 1/1 will both be week "1" if they are in the same week
        if comp1.weekOfYear != comp2.weekOfYear {
            return false
        }
        // Must have a time interval under 1 week
        return abs(self.timeIntervalSince(date)) < Date.weekInSeconds()
    }
    
    func isThisWeek() -> Bool
    {
        return self.isSameWeekAsDate(date: Date())
    }
    
    func isThisMonth() -> Bool {
        let date = Date()
        let comp1 = Date.components(fromDate: self)
        let comp2 = Date.components(fromDate: date)
        return (comp1.month == comp2.month)
    }
    
    func isNextWeek() -> Bool
    {
        let interval: TimeInterval = Date().timeIntervalSinceReferenceDate + Date.weekInSeconds()
        let date = Date(timeIntervalSinceReferenceDate: interval)
        return self.isSameYearAsDate(date: date)
    }
    
    func isLastWeek() -> Bool
    {
        let interval: TimeInterval = Date().timeIntervalSinceReferenceDate - Date.weekInSeconds()
        let date = Date(timeIntervalSinceReferenceDate: interval)
        return self.isSameYearAsDate(date: date)
    }
    
    func isSameYearAsDate(date: Date) -> Bool
    {
        let comp1 = Date.components(fromDate: self)
        let comp2 = Date.components(fromDate: date)
        return (comp1.year == comp2.year)
    }
    
    func isThisYear() -> Bool
    {
        return self.isSameYearAsDate(date: Date())
    }
    
    func isNextYear() -> Bool
    {
        let comp1 = Date.components(fromDate: self)
        let comp2 = Date.components(fromDate: Date())
        return (comp1.year! == comp2.year! + 1)
    }
    
    func isLastYear() -> Bool
    {
        let comp1 = Date.components(fromDate: self)
        let comp2 = Date.components(fromDate: Date())
        return (comp1.year! == comp2.year! - 1)
    }
    
    func isEarlierThanDate(date: Date) -> Bool
    {
        if self < date {
            return true
        } else {
            return false
        }
        let earlierDate = date < self ? self : date
        return earlierDate == self && !(self == date)
    }
    
    func isLaterThanDate(date: Date) -> Bool
    {
        return self > date && self != date
    }
    
    func isLaterThanOrEqualToDate(date: Date) -> Bool
    {
        if self == date {
            return true
        }
        let laterDate = date < self ? date : self
        return laterDate == self
    }
    
    func isEarlierThanOrEqualToDate(date: Date) -> Bool {
        if self == date {
            return true
        }
        let earlierDate = date < self ? self : date
        return earlierDate == self
    }
    
    func isBetween(startDate date: Date, endDate: Date) -> Bool {
        if self.isLaterThanOrEqualToDate(date: date) && self.isEarlierThanOrEqualToDate(date: endDate) {
            return true
        }
        return false
    }
    
    // MARK: Adjusting Dates
    
    func dateByAddingMonths(months: Int) -> Date
    {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
    
    func dateByAddingWeeks(weeks: Int) -> Date
    {
        return Calendar.current.date(byAdding: .weekOfYear, value: weeks, to: self)!
    }
    
    func dateByAddingYears(years: Int) -> Date
    {
        return Calendar.current.date(byAdding: .year, value: years, to: self)!
    }
    
    func dateByAddingDays(days: Int) -> Date
    {
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate + Date.dayInSeconds() * Double(days)
        return Date(timeIntervalSinceReferenceDate: interval)
    }
    
    func dateBySubtractingDays(days: Int) -> Date
    {
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate - Date.dayInSeconds() * Double(days)
        return Date(timeIntervalSinceReferenceDate: interval)
    }
    
    func dateByAddingHours(hours: Int) -> Date
    {
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate + Date.hourInSeconds() * Double(hours)
        return Date(timeIntervalSinceReferenceDate: interval)
    }
    
    func dateBySubtractingHours(hours: Int) -> Date
    {
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate - Date.hourInSeconds() * Double(hours)
        return Date(timeIntervalSinceReferenceDate: interval)
    }
    
    func dateByAddingMinutes(minutes: Int) -> Date
    {
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate + Date.minuteInSeconds() * Double(minutes)
        return Date(timeIntervalSinceReferenceDate: interval)
    }
    
    func dateBySubtractingMinutes(minutes: Int) -> Date
    {
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate - Date.minuteInSeconds() * Double(minutes)
        return Date(timeIntervalSinceReferenceDate: interval)
    }
    
    func dateAtStartOfDay() -> Date
    {
        var components = self.components()
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func dateAtEndOfDay() -> Date
    {
        var components = self.components()
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Calendar.current.date(from: components)!
    }
    
    func dateAtStartOfWeek() -> Date
    {
        let flags: Set<Calendar.Component> = [.year, .month, .weekOfMonth, .weekday]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.weekday = 1 // Sunday
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func dateAtEndOfWeek() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .weekOfMonth, .weekday]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.weekday = 7 // Saturday
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Calendar.current.date(from: components)!
    }
    
    func dateAtEndOfYear() -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 31
        components.month = 12
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Calendar.current.date(from: components)!
    }
    
    func dateAtStartOfNextMonth() -> Date
    {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 1
        components.month = components.month! + 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func dateAtEndOfMonth() -> Date
    {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 0
        components.month = components.month! + 1
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Calendar.current.date(from: components)!
    }
    
    func dateAtStartOfMonth() -> Date
    {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func dateAtStartOfYear() -> Date
    {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = 1
        components.month = 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func dateBySettingWeekDayThisWeek(weekDay: Int) -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .weekday, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        let currentWeekday = self.weekday()
        components.day = components.day! - (currentWeekday - weekDay)
        components.weekday = weekDay
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func dateBySettingDay(day: Int) -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.day = day
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func dateBySettingMonth(month: Int) -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.month = month
        components.day = 1
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func dateBySettingTime(hour: Int, minute: Int) -> Date {
        let flags: Set<Calendar.Component> = [.year, .month, .day]
        var components = Calendar.current.dateComponents(flags, from: self)
        components.hour = hour
        components.minute = minute
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func isLastDayOfMonth() -> Bool {
        let calendar = NSCalendar.current
        let dayRange = calendar.range(of: .day, in: .month, for: self)
        let dayCount = dayRange!.upperBound - dayRange!.lowerBound
        var components = calendar.dateComponents([.year, .month, .day], from: self)
        
        components.day = dayCount
        
        let currentday = self.day()
        return components.day == currentday
    }
    
    // MARK: Retrieving Intervals
    
    func minutesAfterDate(date: Date) -> Int
    {
        let interval = self.timeIntervalSince(date)
        return Int(interval / Date.minuteInSeconds())
    }
    
    func minutesBeforeDate(date: Date) -> Int
    {
        let interval = date.timeIntervalSince(self)
        return Int(interval / Date.minuteInSeconds())
    }
    
    func hoursAfterDate(date: Date) -> Int
    {
        let interval = self.timeIntervalSince(date)
        return Int(interval / Date.hourInSeconds())
    }
    
    func hoursBeforeDate(date: Date) -> Int
    {
        let interval = date.timeIntervalSince(self)
        return Int(interval / Date.hourInSeconds())
    }
    
    func daysAfterDate(date: Date) -> Int
    {
        let interval = self.timeIntervalSince(date)
        return Int(interval / Date.dayInSeconds())
    }
    
    func daysBeforeDate(date: Date) -> Int
    {
        let interval = date.timeIntervalSince(self)
        return Int(interval / Date.dayInSeconds())
    }
    
    
    // MARK: Decomposing Dates
    
    func nearestHour () -> Int {
        let halfHour = Date.minuteInSeconds() * 30
        var interval = self.timeIntervalSinceReferenceDate
        if  self.seconds() < 30 {
            interval -= halfHour
        } else {
            interval += halfHour
        }
        let date = Date(timeIntervalSinceReferenceDate: interval)
        return date.hour()
    }
    
    func year () -> Int { return self.components().year!  }
    func month () -> Int { return self.components().month! }
    func week () -> Int { return self.components().weekOfYear! }
    func weekOfMonth () -> Int { return self.components().weekOfMonth! }
    func weekOfYear () -> Int { return self.components().weekOfYear! }
    func day () -> Int { return self.components().day! }
    func hour () -> Int { return self.components().hour! }
    func minute () -> Int { return self.components().minute! }
    func seconds () -> Int { return self.components().second! }
    func weekday () -> Int { return self.components().weekday! }
    func nthWeekday () -> Int { return self.components().weekdayOrdinal! } //// e.g. 2nd Tuesday of the month is 2
    func monthDays () -> Int {
        let range = Calendar.current.range(of: .day, in: .month, for: self)!
        return range.upperBound - range.lowerBound
    }
    func firstDayOfWeek () -> Int {
        let distanceToStartOfWeek = Date.dayInSeconds() * Double(self.components().weekday! - 1)
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate - distanceToStartOfWeek
        return Date(timeIntervalSinceReferenceDate: interval).day()
    }
    func lastDayOfWeek () -> Int {
        let distanceToStartOfWeek = Date.dayInSeconds() * Double(self.components().weekday! - 1)
        let distanceToEndOfWeek = Date.dayInSeconds() * Double(7)
        let interval: TimeInterval = self.timeIntervalSinceReferenceDate - distanceToStartOfWeek + distanceToEndOfWeek
        return Date(timeIntervalSinceReferenceDate: interval).day()
    }
    func isWeekday() -> Bool {
        return !self.isWeekend()
    }
    func isWeekend() -> Bool {
        let range = Calendar.current.maximumRange(of: .weekday)!
        return (self.weekday() == (range.upperBound - range.lowerBound) || self.weekday() == (range.upperBound - range.lowerBound) - 1)
    }
    
    
    // MARK: To String
    
    func toString() -> String {
        return self.toString(dateStyle: .short, timeStyle: .short, doesRelativeDateFormatting: false)
    }
    
    func toString(format: DateFormat) -> String
    {
        var dateFormat: String
        switch format {
        case .DotNet:
            let offset = NSTimeZone.default.secondsFromGMT() / 3600
            let nowMillis = 1000 * self.timeIntervalSince1970
            return  "/Date(\(nowMillis)\(offset))/"
        case .ISO8601:
            dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        case .RSS:
            dateFormat = "EEE, d MMM yyyy HH:mm:ss ZZZ"
        case .AltRSS:
            dateFormat = "d MMM yyyy HH:mm:ss ZZZ"
        case .Custom(let string):
            dateFormat = string
        }
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        return formatter.string(from: self)
    }
    
    func toString(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style, doesRelativeDateFormatting: Bool = false) -> String
    {
        let formatter = DateFormatter()
        formatter.dateStyle = dateStyle
        formatter.timeStyle = timeStyle
        formatter.doesRelativeDateFormatting = doesRelativeDateFormatting
        return formatter.string(from: self)
    }
    
    func toExpirationStringInFuture(futureDate: Date) -> String {
        let compareComponents = Calendar.current.dateComponents(Date.basicComponentFlags(), from: self, to: futureDate)
//TODO:        let compareComponents = Calendar.current.dateComponents(components: Date.basicComponentFlags(), fromDate: self, toDate: futureDate, options: NSCalendar.Options.WrapComponents)
        
        var stringToReturn = ""
        
        if compareComponents.year! > 0 {
            stringToReturn += "\(compareComponents.year) "
            if compareComponents.year == 1 {
                stringToReturn += "Year"
            } else {
                stringToReturn += "Years"
            }
            // if expiration date is in years then we probably don't need to show anymore precision than that
            return stringToReturn
        }
        
        if compareComponents.month! > 0 {
            stringToReturn += "\(compareComponents.month) "
            if compareComponents.month == 1 {
                stringToReturn += "Month"
            } else {
                stringToReturn += "Months"
            }
            // if expiration date is in months then we probably don't need to show anymore precision than that
            return stringToReturn
        }
        
        if compareComponents.day! > 0 {
            stringToReturn += "\(compareComponents.day) "
            if compareComponents.day == 1 {
                stringToReturn += "Day"
            } else {
                stringToReturn += "Days"
            }
            return stringToReturn
        }
        
        if compareComponents.day == 0 {
            // expires today
            return "Today"
        }
        
        // now check for minus expiration dates
        
        if compareComponents.year! < 0 {
            stringToReturn += "\(compareComponents.year! * -1) "
            if compareComponents.year == -1 {
                stringToReturn += "Year ago"
            } else {
                stringToReturn += "Years ago"
            }
            // if expiration date is in years then we probably don't need to show anymore precision than that
            return stringToReturn
        }
        
        if compareComponents.month! < 0 {
            stringToReturn += "\(compareComponents.month! * -1) "
            if compareComponents.month == -1 {
                stringToReturn += "Month ago"
            } else {
                stringToReturn += "Months ago"
            }
            // if expiration date is in months then we probably don't need to show anymore precision than that
            return stringToReturn
        }
        
        if compareComponents.day! < 0 {
            stringToReturn += "\(compareComponents.day! * -1) "
            if compareComponents.day == -1 {
                stringToReturn += "Day ago"
            } else {
                stringToReturn += "Days ago"
            }
            return stringToReturn
        }
        
        return stringToReturn
    }
    
    func relativeTimeToString() -> String
    {
        let time = self.timeIntervalSince1970
        let now = Date().timeIntervalSince1970
        
        let seconds = now - time
        let minutes = round(seconds/60)
        let hours = round(minutes/60)
        let days = round(hours/24)
        
        if seconds < 10 {
            return NSLocalizedString("just now", comment: "relative time")
        } else if seconds < 60 {
            return NSLocalizedString("\(seconds) seconds ago", comment: "relative time")
        }
        
        if minutes < 60 {
            if minutes == 1 {
                return NSLocalizedString("1 minute ago", comment: "relative time")
            } else {
                return NSLocalizedString("\(minutes) minutes ago", comment: "relative time")
            }
        }
        
        if hours < 24 {
            if hours == 1 {
                return NSLocalizedString("1 hour ago", comment: "relative time")
            } else {
                return NSLocalizedString("\(hours) hours ago", comment: "relative time")
            }
        }
        
        if days < 7 {
            if days == 1 {
                return NSLocalizedString("1 day ago", comment: "relative time")
            } else {
                return NSLocalizedString("\(days) days ago", comment: "relative time")
            }
        }
        
        return self.toString()
    }
    
    func dayWithOrdinalIndicatorString() -> String {
        let day = self.day()
        switch day {
        case 1, 21, 31:
            return "\(day)st"
            
        case 2, 22:
            return "\(day)nd"
            
        case 3, 23:
            return "\(day)rd"
            
        default:
            return "\(day)th"
        }
    }
    
    
    func weekdayToString() -> String {
        let formatter = DateFormatter()
        return formatter.weekdaySymbols[self.weekday()-1] 
    }
    
    func shortWeekdayToString() -> String {
        let formatter = DateFormatter()
        return formatter.shortWeekdaySymbols[self.weekday()-1] 
    }
    
    func veryShortWeekdayToString() -> String {
        let formatter = DateFormatter()
        return formatter.veryShortWeekdaySymbols[self.weekday()-1] 
    }
    
    func monthToString() -> String {
        let formatter = DateFormatter()
        return formatter.monthSymbols[self.month()-1] 
    }
    
    func shortMonthToString() -> String {
        let formatter = DateFormatter()
        return formatter.shortMonthSymbols[self.month()-1] 
    }
    
    func veryShortMonthToString() -> String {
        let formatter = DateFormatter()
        return formatter.veryShortMonthSymbols[self.month()-1] 
    }
}

/******************************************************
End Date Extensions
******************************************************/

/*****************************************************/
//MARK: - UITabBarController Extensions
/*****************************************************/

extension UITabBarController {
    func removeItemTitles() {
        for item in tabBar.items ?? [] {
            item.title = ""
            item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        }
    }
}

/******************************************************
 End UITabBarController Extensions
 ******************************************************/




